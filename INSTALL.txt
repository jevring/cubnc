Welcome to cubnc2! This document details the installation proceedure, as well as the configuation choices you have to make.
Below you will find a series of steps that you need to follow to install cubnc and get it up and running.
For information on running and using cubnc, please see the README.txt file.

* Install Java
* Unpack cubnc
* Configure userbase
* Configure your certificates
* Configure slaves
* Configure cubnc


--- Install java:
Windows/Linux/Solaris:
- Download JDK 1.7 from: http://java.sun.com/javase/downloads/
- Get the executable for your particular version and architecture
- Follow the instructions
Apple:
- Should have some JDK built-in, as far as I know.
*BSD:
- See instructions on: http://www.eyesbeyond.com/freebsddom/java/jdk16.html

--- Unpack cubnc:
- Choose a suitable unpacking application for your OS
- Unpack cubnc to a location of your choosing

--- Configure userbase:
You have several choices of userbases. Currently the only one that allows you to use both usernames and groups with the
built-in permission system is cuftpd-remote. All other userbases allow only for usernames to be used with the permission
system. You choose a userbase by setting the '/bnc/user/authentication/type' setting in data/bnc.xml to the appropriate
value as indicated in the comments in that file.

* cuftpd-remote - This option uses RMI to connect to a remote cuftpd userbase. To do this, you have to
exchange certificates between the cuftpd userbase and the cubnc instance, otherwise the remote userbase will not allow
your connection. See the "Setting up certificates for a remote cuftpd userbase" section. This option is recommended,
because it is very fast, and as cubnc is largely based on cuftpd, you get centralized access to all the user control
commands, such as adding, deleting and modifying users. This also lets you view transfer statistics for users from all
the connected slaves, provided that they are set up to use remote statistics. Using this userbase works very well in
conjunction with slaves running cuftpd, as they will shave the same remote userbase. One might say that they were made
for each other ;)

* redundant host authenticator - This userbase authenticates users by connecting to a remote host and using the credentials
of the users that connect to cubnc to log on. If the log on to the remote system succeeds, the log on to cubnc succeeds.
This userbase does not provide access to any user control functionality, unlike the cuftpd-remote userbase mentioned above.

* glftpd 1.32/2.01 userbase - This userbase uses locally (or via nfs, for instance) accessible userbase files from glftpd
1.32 or 2.01 (you have to choose) to perform authentication. It looks in the passwd-file for passwords and in the userfiles
for ip-restrictions. Some ftp clients have a requirement (some allow this requirement to be turned off, like flashfxp)
that the certificate used for the control connection is the same that is used for the data connection. As glftpd doesn't
know how to work with the key file format that Java uses, you have to export the keys from the Java files to a format
that glftpd can read. To do this, please see the "Setting up certificates for glftpd slaves" section.

* anonymous - lets anybody in with no restrictions. Does not require username or password. No access to statistics or
user control

* cuftpd-local (not recommended) - This userbase used the cuftpd userfiles on localhost. It is not recommended due to
the fact that you cannot sync userbase changes between hosts when using this userbase.

* asynchronous (recommended) - uses the cuftpd asynchronous syncing database. Changes are replicated to all peers to
ensure that they stay in sync.

--- Configure your certificates:
To enable encrypted connections to cubnc, you have to create a certificate and configure cubnc to use it. Depending on
which userbase you choose to use, there may be extra steps, but this step is required for all installations. To create
this certificate, execute the following:

#keytool -genkey -v -keyalg RSA -keystore client.keystore -dname "CN=Client, OU=Bar, O=Foo, L=Some, ST=Where, C=UN"

This creates a file called "client.keystore", which you can then instruct cubnc to use. By default cubnc looks for a file
called "client.keystore" with the password "client" in the data/ directory in the cubnc installation directory. If you
want to use another name, password or location, you have to indicate these in data/bnc.xml

--- Configure slaves:
For some configuations, you have to add special configuration to the slaves.

* cuftpd slaves - You have to change the <bouncers> setting in data/cuftpd.xml to include the hostname(s) of the cubnc
bouncers, otherwise IP-checking (via the IDNT command) will not work.

* glftpd slaves - You have to add either -b or -B to the glftpd command line, otherwise IP-checking (via the IDNT command)
will not work. You also have to alter /etc/glftpd.conf (it can be in other locations, such as /jail/glftpd/etc/glftpd.conf)
to change (or include) the bouncer_ip setting to include the hostname(s) of the cubnc bouncer(s).

--- Configure cubnc:

- edit 'data/bnc.xml' to reflect the settings you want, such as ports and virtual directory structure. This file contains
  plenty of comments to guide you.
- edit 'data/permissions.acl' to reflect the permissions you want (information provided in the file)
- after this, all the configuration is done online via "site"-commands.

--- Setting up certificates for a remote cuftpd userbase:
To connect to a remote cuftpd userbase, you have to import the cubnc certificate in the keystore of the userbase and
vice versa. The section below is taken from the cuftpd README.txt and details how this should be done. This is used to
set up the initial keystore for the userbase as well. If you have already set up the keystore for the remote userbase,
you want to only perform steps 2 and 3, provided that you have created a keystore called "client.keystore" for use in
cubnc already, as described in the "Configure your certificates" section.

Most of this information is taken from the "Key Management" section of http://www.cs.columbia.edu/~akonstan/rmi-ssl/
It does a good job of detailing what needs to be done. The "server" parts refer to the remote userbase, and the "client"
parts refer to computer running cubnc. The command can all be executed on the same machine, and then the "server.keystore"
file can be moved to the computer running the remote userbase. When executing these commands, you will be asked to supply
a password. Both when creating the keystores and when adding the public keys to them.

1.  Create a self-signed server and a self-signed client key each in its own keystore
    keytool -genkey -v -keyalg RSA -keystore server.keystore -dname "CN=Server, OU=Bar, O=Foo, L=Some, ST=Where, C=UN"
    keytool -genkey -v -keyalg RSA -keystore client.keystore -dname "CN=Client, OU=Bar, O=Foo, L=Some, ST=Where, C=UN"

2. Export the server's and the client's public keys from their respective keystores
    keytool -export -rfc -keystore server.keystore -alias mykey -file server.public-key
    keytool -export -rfc -keystore client.keystore -alias mykey -file client.public-key

3. Import the client's public key to the server's keystore, and vice-versa:
    keytool -import -alias client -keystore server.keystore -file client.public-key
    keytool -import -alias server -keystore client.keystore -file server.public-key
(Answer "yes" when asked if you want to import)

When adding more servers to the set of hosts connecting to the remote userbase, you will repeat all but the first command.
This ensures that all your clients will trust the remote userbase, and that the remote userbase knows which clients to trust.
Clients without this dual trust will not be allowed to connect to the remote userbase.

When adding more servers, create the client keystore, extract the client public key (all as above), then copy the public
key to the computer running the remote userbase, and use the method above to import it to the server.keystore.

Normally the remote userbase will use "server.keystore" as both keystore and truststore, however it is possible to use
a different file as a truststore, thus the different settings in userbase.conf.
A similar situation exists for the clients (cuftpd server instances).

NOTE: The server.keystore file is ONLY CREATED ONCE and then you ADD public keys from cuftpd servers to it.
The client.keystore file is created FOR EACH SERVER, and then you ADD the REMOTE USERBASE PUBLIC KEY to client.keystore.

--- Setting up certificates for glftpd slaves:

HOW-TO use the same certificate with glftpd (or other ftp+tls enable daemon) and java

This section details how to use the same encryption certificates (private and public key) on both the bnc and the servers.
You might think that "this is a piece of cake, just copy the ftp-dsa.pem from the server to the bnc". This, however,
for many reasons do not work. For example, java keystores use the DER format to store its keys, and openssl uses PEM.

Anyway, there is a way around this, and when you know how to, it's fairly simple to execute.
You begin by creating the keystore you want to use with cubnc in the way specified in README.txt:

hostname# keytool -genkey -alias cuBnc -keypass cubncpw -keystore client.keystore -storepass cubncpw -keyalg RSA -keysize 2048
(You can use 1024 as the keysize aswell, it's up to you, and the speed of your machine)

You then export the private and public key from the keystore:

public key:

hostname# keytool -export -rfc -alias cubnc -keystore client.keystore -keypass cubncpw -storepass cubncpw -file pubkey.crt

private key:

hostname# java -jar keyexport.jar client.keystore mykey cubncpw > privkey.key
NOTE: "mykey" is the default name for keys created in a keystore. If you gave your key another alias, use that here)

Then you put the private key and the public key in a certificate file

hostname# cat privkey.key pubkey.crt > server.pem

Then you exchange your current certificate for server.pem. The default way to do this in glftpd is:
NOTE: This needs to be done for ALL the slaves. I can't stress this enough. Every single last one of them needs THIS
certificate, not one made like it, but THIS! So transfer and copy and cat on all slaves.

hostname# cat server.pem > /etc/ftpd-dsa.pem

Finally you copy client.keystore to your cubnc/data dir, and make sure cubnc/data/bnc.xml points to it, and that the password is correct.
Note that you can use whatever names you like for the keystore and password, HOWEVER both the -storepass and the -keypass
MUST be the same, otherwise it will NOT work.

References:

http://javaalmanac.com/egs/java.security/GetKeyFromKs.html
http://forum.java.sun.com/thread.jspa?forumID=2&messageID=449486&threadID=154587
http://mark.foster.cc/kb/openssl-keytool.html
