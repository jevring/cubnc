The *_OWN permissions do NOT work in cubnc, due to the fact that it is difficult (and sometimes impossible) to reliably get this information from a slave.

--- Getting help ---

Try the "site help" or "site help $command" commands when logged on to the ftpd.

--- Running cubnc ---

#java [-server] -jar cubnc-$VERSION.jar
The "-server" is optional. It can improve performance in some cases. It affects the policies regarding memory, mostly.
The "-server" switch is only available if you have the full JDK, not when you run just the JRE.

When you see "cubnc $VERSION online", where $VERSION is the version you are currently running, cubnc is online.
cubnc will NOT fork into the background, so you need to do that yourself in an appropriate manner.

--- NEW - Running cubnc with an ASYNCHRONOUS remove userbase ---
This is a new cubnc feature in 2.0-beta5 and later. The asynchronous userbase lets your cubnc instance operate as if it were local,
and simply update its peers asynchronously in the background. This works even if one of the peers goes offline for
a time, provided if was configured as a peer. Simply choose authentication type 4, and configure your peers (other cuftpd
or cubnc instances) in bnc.xml and you are good to go.
IMPORTANT: You must ensure that no loops are set up in the peer structure. Having a tree-like structure is fine. Circles are not.

If you are just starting out with empty userbases for all peers, simply connecting them via peer configuration as specified above.
If, however, you already have a userbase one a peer, or perhaps you want to add a new peer to an existing network of peers, then
you must do the following: Shut down one of the running peers that you intend to connect the new peer to. Make a copy of the stopped
peer's userbase, and copy it to the new peer. Then start them back up. This should provide them both with a common sync point.
It is important that you copy the userbase BEFORE you start them up as peers for the first time, because otherwise any pending
changes will be in the queue, and will cause inconsistencies.
"But what if I don't want to shut down my running hosts?", you ask. Well, you have to, to add a peer, because every peer-relationship
is two-sided, so you have to restart one of the peers anyway, after having changed its configuration, so there is little you can do.
In the future, it will perhaps be possible to forgo this manual step, but for now, it is necessary.

--- A note on certificates ---

The certificates created by the methods below will have a default expiration time of 90 days. This can be altered by
using different command line switches. I would suggest using more or less "real" values for the dname, such as
maybe a hostname for the CN, or a correct IP. This will help you identify and keep track of the certificates
when you start juggling more than one set.
Using correct information might also prevent dialog boxes from popping up for ftp client users if the CN matches
the hostname in question.

--- Configuring permissions in cubnc ---

Some commands in cubnc requires that the user have certain permissions. These are different from the permissions
in data/permissions.acl, in that they are not path bound, and are connected to the userfile itself.
When requesting help for a certain command, it will say if any permission is required, and if so, which.
These permissions will be described with names. These names can be resolved to numbers by executing "site permissions".
When granting or removing a permission from a user, use the commands "site addpermissions" and "site delpermissions".
These commands take as parameters the user in question, and the permissions to be added or removed.
Permissions can either be a sole number, or a comma-separated list of numbers, such as: 1,2,3,4,5,6,7
To see a user's permission, execute "site user username".

Note: the permissions contained in the 'data/permissions.acl' file are reloaded each time someone logs in, so there is
no need to restart the server just because the permissions changed.

--- Note on IPv6 ---

Because java supports IPv6 out-of-the-box, cubnc supports IPv6 if the underlying operating system supports IPv6.
Currently most major operating systems have default or enableable support for IPv6.
cubnc also supports the EPSV and EPRT commands.

--- Custom site commands ---

If you want to add custom site commands, then see the /bnc/commands section in data/bnc.xml.
You can add custom site commands there that will invoke external scripts for you.
NOTE: Choose the names of the commands with care, as they will overwrite any original site commands with the same name.

The processes are sent the following parameters in the following order:

  $1 = username
  $2 = primary group
  $3 = tagline
  $4 = current section name
  $5 = current path (absolute in the file system)
  $6 = command parameters

where 'command parameters' indicate any parameters that were sent to the command itself via the ftp.
Note that these will parameters will show up as one variable. 'command parameters' will also include the name of the site command.

Thus, if you are invoking some external script called "/cubnc/bin/pre.sh", then the command line will look as follows:

# /cubnc/bin/pre.sh captain some_group "I have no tagline" mp3 "/cubnc/site/mp3/some/dir" "site pre blah kek lol"

To deliver help for these commands to your users, put a file named "command.txt" in data/help/, where 'command' is the
name of the command.

You can also use java classes as custom site commands with cubnc.
Just create a class that extends cu.ftpd.commands.site.actions.Action. In this class you can override the execute(..)
method. To use java classes as custom site commands, you have to set the <type> tag to "java".
For more information, just look at any of the classes in cu.ftpd.site.actions.*, and it should become clear what to do.
NOTE: when using type=java, the add_response_code parameter will do nothing. Your classes have to provide a response
code by themselves

--- 7bit templates for site output ---

If your output looks weird, look in the FAQ for information on how to change your display templates.

--- CRITICAL errors ---

When you get errors tagged with CRITICAL in your error log, it's a sure sign that something is VERY wrong. Therefore,
I suggest checking it every now and then, or doing a selective 'tail' of it, so you can see when errors happen.
Some critical errors are recoverable, and some are not. If they are recoverable, that means that the system will keep
running, but the problem needs to be looked at right away.

--- Compiling cubnc ---

The normal distribution comes with jar-files that you can use. If you want to recompile the source code, the easiest
way is to download maven from http://maven.apache.org/ and use that in conjunction with the 'pom.xml' file included
in the cubnc distribution, and do "mvn clean install".

--- Handling events ---

cubnc comes with an event handling system. For some selected events (listed below), it will trigger event handlers
both "before" and "after" the event in question. These event handlers can be either java classes or shell scripts.
Handlers for "before" events have the ability to write to the control connection. If you want to send a message to
the user, this is the only way.
NOTE: IF you want to write to the control connection you MUST include ftp response codes. It is crucial that these codes
correspond to the respond code of the original command, if the command is to continue. I.e. if you want to echo something
before an MKD, then you HAVE to respond with 257 if it is good.
Also note that all lines in a multiple line respond have a "-" just after the 3-digit number. The last line does not have
this, indicating the end of the message. This information is available in RFC 959 (google it).
If you want to halt execution of the command, you do this by returning false in the case of the java handlers, and
anything other than 0 for the shell handlers.
Halting execution means that any other event handlers will also not be run. You can have multiple event handlers for
each event. Some events have a set of pre-defined event handlers that are initialized in the code. These are automatically
run before any user-defined event handlers. These pre-defined handlers include things like dupecheck and dirlog.

Java classes:
All java class event handlers must implement the AfterEventHandler and/or BeforeEventHandler as appropriate.
These interfaces reside in src/cu/ftpd/events/. These interface have javadoc comments about what implementing
them means, and how they work. Each class will only be instantiated once, so if you have multiple event handlers
that use the same class, they will all be invoked on the same object.
Upon triggering an event, these event handlers will receive the event in question. Different events contain different
types of data. What this data is can be seen where the events are issued, for instance in the Connection, CommadnSTOR
and CommandRETR classes. The events currently in use are:

    Event name                  Event id
    ------------------------------------
    CREATE_DIRECTORY            0
    REMOVE_DIRECTORY            1
    DELETE_FILE                 2
    RENAME_FROM                 3
    RENAME_TO                   4
    SITE_COMMAND                100
    UPLOAD                      1000
    DOWNLOAD                    1001

Shell scripts/applications:
These can be any shell scripts or applications that are executable on the host system.
These scripts receive a different set of parameters depending on what event is triggered.
They also receive a string indicating "before" or "after" ("time" below), as well as an interger event type id.
This event type id is the one described in the table above.

    All events share these parameters:
    $1 = time
    $2 = event type
    $3 = username
    $4 = group
    $5 = tagline
    $6 = pwd (absolute path)
    $7 = pwd (path relative to the ftp root)

    After that the events send these commands, respectively

    RENAME_TO, RENAME_FROM:
    $8 = source (absolute path)
    $9 = source (path relative to the ftp root)
    $10 = target (absolute path)
    $11 = target (path relative to the ftp root)
    $12 = directory (boolean)
    $13 = file size (long)


    UPLOAD, DOWNLOAD:
    $8 = state (0 = PENDING, 1 = COMPLETE)
    $9 = bytes transferred
    $10 = transfer time
    $11 = transfer type
    $12 = remote hostname

    DELETE_FILE:
    $8 = file (absolute path)
    $9 = file (path relative to the ftp root)
    $10 = file size

    SITE_COMMAND:
    $8 = site command parameters (e.g. "WIPE /some/dir/")

    All others contain:
    $8 = file (absolute path)
    $9 = file (path relative to the ftp root)

