package cu.bnc.user.userbases.glftpd;

import cu.ftpd.user.User;
import org.junit.Test;

import java.io.File;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.*;

public class GlftpdUserbaseTest {

	@Test
	public void testGetUser() throws Exception {
		GlftpdUserbase glftpdUserbase = new GlftpdUserbase(GlftpdUserbase.GLFTPD_201, new File("."), new File("passwd"));
		GlftpdUserbase.UserInfo user = glftpdUserbase.readUserFile(getClass().getResourceAsStream("user"));
		assertEquals("people", user.getGroup());
		assertEquals("No Tagline Set", user.getTagLine());
		assertEquals(Collections.singletonList("*@192.168.0.1"), user.getIps());
		
		// this is cheating, constructing the actual value is a pain in the ass
		assertEquals(new Date(2529961200L * 1000), user.getExpires());
		// expires in 2050, so these tests should succeed for a while now
		assertTrue(user.getExpires().after(new Date()));
	}
}