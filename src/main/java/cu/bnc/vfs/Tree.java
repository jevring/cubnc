/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.vfs;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-18 - 17:43:49
 */
public class Tree {
    protected final Tree parent;
    protected final String name;
    protected final String owner;
    protected final String group;
    protected final String path;
    protected String slaveName;
    protected String slavePath;
    protected final Map<String, Tree> children = new HashMap<String, Tree>();
    protected boolean alias;
    protected long lastModified = System.currentTimeMillis();

    public Tree(Tree parent, String name, String owner, String group) {
        this.parent = parent;
        this.name = name;
        this.owner = owner;
        this.group = group;
        // this inline condition takes care of the root situation
        this.path = (parent == null ? "" : parent.getPath()) + name + "/";
    }

    public void setSlaveName(String slaveName) {
        this.slaveName = slaveName;
    }

    public void setSlavePath(String slavePath) {
        this.slavePath = slavePath;
    }

    public void setAlias(boolean alias) {
        this.alias = alias;
    }

    public void addChild(String name, Tree child) {
        children.put(name, child);
    }

    public Tree getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getGroup() {
        return group;
    }

    /**
     * Returns the "real" (bnc) path of this tree.
     * @return the "real" (bnc) path of this tree.
     */
    public String getPath() {
        return path;
    }

    public boolean isAlias() {
        return alias;
    }

    public Tree getChild(String name) {
        return children.get(name);
    }

    public String getSlavePath() {
        return slavePath;
    }

    public String getSlaveName() {
        return slaveName;
    }

    public Map<String, Tree> getChildren() {
        return children;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
        if (parent != null) {
            parent.setLastModified(lastModified);
        }
    }

	@Override
	public String toString() {
		return "Tree{" +
				"path='" + path + '\'' +
				", slaveName='" + slaveName + '\'' +
				", slavePath='" + slavePath + '\'' +
				", alias=" + alias +
				'}';
	}
}
