/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.vfs;

import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.util.Map;
import java.util.HashMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-18 - 17:43:25
 */
public class VirtualDirectoryTree {
    protected Tree root = new Tree(null, "", "cubnc", "cubnc");
    protected final Map<String, String> slavePathToRealPath = new HashMap<String, String>();

    public VirtualDirectoryTree(Node virtualDirs) {
        build(root, virtualDirs.getChildNodes());
    }

    protected void build(Tree parent, NodeList children) {
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            //System.out.println(child);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                NamedNodeMap attributes = child.getAttributes();
                String name = attributes.getNamedItem("name").getNodeValue();
                Node owner = attributes.getNamedItem("owner");
                Node group = attributes.getNamedItem("group");
                Tree tree = new Tree(parent,
                        name,
                        (owner == null ? parent.getOwner() : owner.getNodeValue()),
                        (group == null ? parent.getGroup() : group.getNodeValue())
                );
                parent.addChild(name, tree);
                if (child.getNodeName().equalsIgnoreCase("dir")) {
                    tree.setAlias(false);
                    build(tree, child.getChildNodes());
                } else if (child.getNodeName().equalsIgnoreCase("alias")) {
                    Node slave = attributes.getNamedItem("slave");
                    Node path = attributes.getNamedItem("path");
                    tree.setSlaveName(slave.getNodeValue());
                    tree.setSlavePath(path.getNodeValue());
                    tree.setAlias(true);
                    slavePathToRealPath.put(tree.getSlavePath(), tree.getPath());
                }
            }
        }
    }

    public Tree getRoot() {
        return root;
    }

    public Map<String, String> getSlavePathToRealPath() {
        return slavePathToRealPath;
    }
}
