/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.vfs;

import cu.authentication.AuthenticationRequest;
import cu.bnc.slave.Slave;
import cu.ftp.FtpConnection;
import cu.ftp.FtpResponse;
import cu.ftp.HostDisconnectedException;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.filesystem.Section;
import cu.ftpd.filesystem.permissions.ActionPermission;
import cu.ftpd.filesystem.permissions.PermissionDeniedException;
import cu.ftpd.user.User;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.AccessControlException;
import java.util.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-19 - 22:08:46
 */
public class VirtualFileSystem extends FileSystem {
    private static final XPath xpath = XPathFactory.newInstance().newXPath();
    protected final VirtualDirectoryTree vtree;
    protected final String password;
    protected static final Map<String, Slave> slaves = new HashMap<>();
    protected Location currentLocation;
    protected final Location root;
    protected final Map<String, FtpConnection> connectionCache = new HashMap<>();
    /**
     * This map contains the properties that need to be transferred to the slaves upon connection, such as PROT etc.
     */
    protected final Map<String, String> properties = new HashMap<>();
    private Location remoteRenameSource = null;
    private boolean encryptedControlConnection = false;
    private final AuthenticationRequest auth;
	private final File workingDirectory;

    public VirtualFileSystem(User user, VirtualDirectoryTree vtree, String password, boolean encryptedControlConnection, AuthenticationRequest auth) {
        super(user);
        this.vtree = vtree;
        this.password = password;
        root = new Location(vtree.getRoot(), null, "/", null); // the root can never be a slave, and it doesn't have a path
        currentLocation = root;
        // NOTE: Therefore, we must remember to concatenate the root with "/" when we return it.
        this.encryptedControlConnection = encryptedControlConnection;
        this.auth = auth;
        FileSystem.defaultSection = new Section("default", "/", "nobody", "nobody", 3);
        this.section = defaultSection;
	    try {
		    this.workingDirectory = new File("").getCanonicalFile();
	    } catch (IOException e) {
		    // should never happen
		    throw new IllegalStateException("Could not determine working directory", e);
	    }
    }

    public static void loadSlaves(XMLSettings settings) throws ConfigurationException {
        try {
            NodeList nodes = settings.getNode("/slaves").getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if ("slave".equals(node.getNodeName())) {
                    Slave slave = createSlave(node);
                    slaves.put(slave.getName(), slave);
                }
            }
        } catch (XPathExpressionException e) {
            throw new ConfigurationException(e.getMessage(), e);
        } 
    }

    private static Slave createSlave(Node node) throws XPathExpressionException {
        // NOTE: we don't have to catch NumberFormatExceptions here, since the XSD has checked that they conform to xs:integer (and equivalent for boolean)
        String name = xpath.evaluate("name", node);
        String host = xpath.evaluate("host", node);
        String portString = xpath.evaluate("port", node);
        int port = 21;
        if (portString != null && !"".equals(portString)) {
            // no need to check this exception, the XSD will guarantee that the value is of the correct type (if it exists)
            port = Integer.parseInt(portString);
        }
        String acceptsIdntString = xpath.evaluate("accepts_idnt", node);
        boolean acceptsIdnt = true;
        if (acceptsIdntString != null && !"".equals(acceptsIdntString)) {
            acceptsIdnt = Boolean.parseBoolean(acceptsIdntString);
        }
        String showWelcomeMessageString = xpath.evaluate("show_welcome_message", node);
        boolean showWelcomeMessage = false;
        if (showWelcomeMessageString != null && !"".equals(showWelcomeMessageString)) {
            showWelcomeMessage = Boolean.parseBoolean(showWelcomeMessageString);
        }
        String connectionTimeoutString = xpath.evaluate("connection_timeout", node);
        int connectionTimeout = 30;
        if (connectionTimeoutString != null && !"".equals(connectionTimeoutString)) {
            connectionTimeout = Integer.parseInt(connectionTimeoutString);
        }
        String timeoutString = xpath.evaluate("timeout", node);
        int timeout = 180;
        if (timeoutString != null && !"".equals(timeoutString)) {
            timeout = Integer.parseInt(timeoutString);
        }
        return new Slave(name, host, port, acceptsIdnt, showWelcomeMessage, connectionTimeout, timeout);
    }

    public static Slave getSlave(String slaveName) {
        return slaves.get(slaveName);
    }

    public static Map<String, Slave> getSlaves() {
        return slaves;
    }

    private String glob(String path) {
        String[] ps = path.split("/");
        Deque<String> stack = new LinkedList<>();
	    for (String s : ps) {
		    if (s.isEmpty() || ".".equals(s)) {
			    continue;
		    } else if ("..".equals(s)) {
			    // remove the last one we added to the stack
			    if (!stack.isEmpty()) { // this lets us do ../ against the root without leaving it
				    stack.pop();
			    }
		    } else {
			    stack.push(s);
		    }
	    }
        if (!stack.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            Iterator<String> i = stack.descendingIterator();
            while (i.hasNext()) {
                sb.append("/").append(i.next());
            }
            return sb.toString();
        } else {
            return "/";
        }
    }

    @Override
    public String resolveRealPath(String ftpPath) {
        try {
            Location location = resolve(ftpPath, currentLocation);
	        if (location.isRemote()) {
		        return location.getSlaveName() + ":" + location.getSlavePath();
	        } else {
		        return "<bnc>:" + location.getPath();
	        }
        } catch (FileNotFoundException e) {
            return "error:" + e.getMessage();
        }
    }

    @Override
    public String resolveFtpPath(String ftpPath) {
	    try {
		    // This is the path of the object ON THE SLAVE
		    Location location = resolve(ftpPath, currentLocation);
		    if (location.isRemote()) {
			    return location.getSlavePath();
		    } else {
			    return location.getPath();
		    }
	    } catch (FileNotFoundException e) {
		    return "/";
	    }
	    
    }

    @Override
    public String getRealRenameSource() {
        if (remoteRenameSource == null) {
            return null;
        } else {
            return remoteRenameSource.getSlaveName() + ":" + remoteRenameSource.getSlavePath();
        }
    }

    @Override
    public String getFtpRenameSource() {
        if (remoteRenameSource == null) {
            return null;
        } else {
            return remoteRenameSource.getPath();
        }
    }

    @Override
    public String getFtpParentWorkingDirectory() {
        return currentLocation.getPath();
    }

    @Override
    public String getRealParentWorkingDirectoryPath() {
	    if (currentLocation.isRemote()) {
		    return currentLocation.getSlaveName() + ":" + currentLocation.getSlavePath();    
	    } else {
		    return workingDirectory.getAbsolutePath();
	    }
    }

	@Override
	public File getPwdFile() {
		return workingDirectory;
	}

	@Override
    public void cwd(String directory) throws PermissionDeniedException, AccessControlException, IOException {
        // relay even ".", since it's an anti-idle
        try {
            Location t = resolve(directory, currentLocation);
            if (t == null) {
                t = root;
            }
            //System.out.println("resolved '" + directory + "' from '" + currentLocation.getPath() + "' to: " + t.getPath());
            if (t.isRemote()) {
                FtpConnection slave = getSlaveConnection(t.getSlaveName());
                boolean success = slave.cwd(t.getSlavePath());
                if (!success) {
                    throw new FileNotFoundException("No such directory: " + directory);
                }
            }
            String path = t.getPath();
            // todo: shouldn't the permissions typically be checked BEFORE we try to log on?
            if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.CWD, path, user)) {
                throw new PermissionDeniedException(ActionPermission.CWD, path, user);
            }
            currentLocation = t;
        } catch (HostDisconnectedException e) {
            throw new IOException("Slave unavailable: " + e.getMessage(), e);
        }
    }

    public Location resolve(String targetPath, Location currentLocation) throws FileNotFoundException {
        Location location = root;
        Tree currentTreeNode = vtree.getRoot();
        if (targetPath == null) {
            // this happens if we do NLST or MLSD, for instance, without parameters
            return currentLocation;
        }
        String globbedPath;
        if (targetPath.startsWith("/")) {
            globbedPath = glob(targetPath);
        } else {
	        globbedPath = glob(currentLocation.getPath() + "/" + targetPath);
        }
        int currentPathPosition = 0;
        String[] pathParts = globbedPath.split("/");
        for (int i = 1; i < pathParts.length; i++) {
            String pathPart = pathParts[i];
            currentPathPosition += pathPart.length() + 1; // add the pathPart plus 1 for the "/" to the current length
            currentTreeNode = currentTreeNode.getChild(pathPart);
            if (currentTreeNode == null) {
                // there was no such directory
                throw new FileNotFoundException(globbedPath);
            } else {
                if (currentTreeNode.isAlias()) {
                    String slavePath = currentTreeNode.getSlavePath();
                    if (slavePath.endsWith("/") && slavePath.length() > 1) {
                        slavePath += globbedPath.substring(currentPathPosition - 1); // skip a slash
                    } else {
                        slavePath += globbedPath.substring(currentPathPosition);
                    }
                    // this location indicates that we are in a slave, and contains all the information needed
                    location = new Location(currentTreeNode, slavePath, globbedPath, currentTreeNode.getSlaveName());
                    break;
                } else {
                    if (i == pathParts.length - 1) {
                        // this location means that we are still somewhere in the bouncer, and not in a slave
                        location = new Location(currentTreeNode, null, globbedPath, null);
                    }
                    // keep trying
                }
            }
        }
        return location;
    }

    @Override
    public List<String> nlst(String path) throws PermissionDeniedException, AccessControlException, FileNotFoundException {
        Location location = resolve(path, currentLocation);
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.LIST, location.getPath(), user)) {
            throw new PermissionDeniedException(ActionPermission.LIST, location.getPath(), user);
        }
        if (location.isRemote()) {
            throw new IllegalStateException("Cannot list() a remote directory through local means");
        } else {
            LinkedList<String> lines = new LinkedList<>();
            for (Tree tree : currentLocation.getTree().getChildren().values()) {
                if (!ServiceManager.getServices().getPermissions().isVisible(user, tree.getName(), getFtpParentWorkingDirectory(), true)) {
                    // this file or directory was hidden from this user, so we don't display it
                    continue;
                }
                lines.add(tree.getName());
            }
            return lines;
        }
    }

    @Override
    public List<String> list(String path) throws AccessControlException, FileNotFoundException, PermissionDeniedException {
        // todo: handle the path here
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.LIST, getFtpParentWorkingDirectory(), user)) {
            throw new PermissionDeniedException(ActionPermission.LIST, getFtpParentWorkingDirectory(), user);
        }
        if (currentLocation.isRemote()) {
            throw new IllegalStateException("Cannot list() a remote directory through local means");
        } else {
            LinkedList<String> lines = new LinkedList<>();
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            Calendar c = Calendar.getInstance();
            String dateline;
            String group;
            String owner;
            for (Tree tree : currentLocation.getTree().getChildren().values()) {
                if (!ServiceManager.getServices().getPermissions().isVisible(user, tree.getName(), getFtpParentWorkingDirectory(), true)) {
                    // this file or directory was hidden from this user, so we don't display it
                    continue;
                }
                c.setTimeInMillis(tree.getLastModified());
                if (c.get(Calendar.YEAR) == currentYear) {
                    // do the listing with time instead of year
                    dateline = time.format(c.getTime());
                } else {
                    // do the listing with year instead of time
                    dateline = year.format(c.getTime());
                }
                // default: (actually, section can never be null, since we always have a default section)
                owner = "cubnc";
                group = "cubnc";

                // next highest default is the parent group metadata
                if (section != null) {
                    owner = section.getOwner();
                    group = section.getGroup();
                }

                if (tree.getOwner() != null) {
                    owner = tree.getOwner();
                }

                if (tree.getGroup() != null) {
                    group = tree.getGroup();
                }

                // note: if we don't do ""+file.length() here, the formatter will insert spaces or commas or other stuff (depending on the locale) to format the number.
                // NOTE: the formatter is EXPENSIVE, but still much simpler than just concatenating, or using a string buffer
                lines.add(listline.format(new String[]{"d", owner, group, String.valueOf(0), dateline, tree.getName(), "w"}));
            }
            return lines;
        }
    }

    public FtpConnection currentSlave() throws HostDisconnectedException, IOException {
        if (currentLocation.isRemote()) {
            return getSlaveConnection(currentLocation.getSlaveName());
        } else {
            System.err.println("currentSlave() returned null!");
            Thread.dumpStack();
            return null;
        }
    }

    public FtpConnection getSlaveConnection(String slaveName) throws IOException, HostDisconnectedException {
        FtpConnection slaveConnection = connectionCache.get(slaveName);
        boolean ok;
        if (slaveConnection == null) {
            Slave slave = slaves.get(slaveName);
            slaveConnection = new FtpConnection(slave.getHost(), slave.getPort(), user.getUsername(), password, encryptedControlConnection, 1 /* 1=AUTH TLS; 2=AUTH SSL; 3=ftps */, slaveName);
            slaveConnection.setConnectionTimeout(slave.getConnectionTimeout());
            slaveConnection.setTimeout(slave.getTimeout());
	        // todo: have a flag that lets us control this from the settings
            //slaveConnection.setDebug(true);
            ok = slaveConnection.connect();
            if (ok) {
                if (slave.acceptsIdnt()) {
                    slaveConnection.send("IDNT " + auth.getIdent() + "@" + auth.getHost().getHostAddress() + ":" + auth.getHost().getHostName());
                    // pass false here, since send() above will already have invoked parseResponse()
                    ok = slaveConnection.logon(false);
                } else {
                    ok = slaveConnection.logon(true);
                }
                if (ok) {
                    // transfer the state
                    if (mode != null) {
                        slaveConnection.send("MODE " + mode);
                    }
                    slaveConnection.send("TYPE " + type.substring(0, 1));
                    if (offset != 0) {
                        slaveConnection.send("REST " + offset);
                    }
                    for (Map.Entry<String, String> property : properties.entrySet()) {
                        slaveConnection.send(property.getKey() + (property.getValue() != null ? " " + property.getValue() : ""));
                    }
                    // only put it in the set of available slaves if we actually managed to connect
                    connectionCache.put(slaveName, slaveConnection);
                }
            }
        } else {
            // _todo: just because it exists doesn't mean that it actually works.
            // _todo: we have to make sure we are connected here before we return it (or try sending a command or something), and if we can't, we have to throw a HostDisconnectedException
            // there's a problem, though, since we call this ALL THE TIME, so we can't issue a check command each damn time
            // we basically have to remove it, or at least not put it in the map if it is not properly connected
            ok = true;
        }
        if (ok) {
            return slaveConnection;
        } else {
            slaveConnection.disconnect(false); // regardless of what the error was, try to disconnect. sending "quit" times out after a second anyway
            throw new HostDisconnectedException("Could not to connect to slave", slaveName);
        }
    }

    /**
     * Reconnects the current slave, after a disconnection, so that the continued connectedness is transparent to the user.
     * @throws cu.ftp.HostDisconnectedException if the reconnection fails
     * @throws java.io.IOException if some IO error occurs during reconnection
     */
    public void reconnect() throws HostDisconnectedException, IOException {
        // this should invalidate the slave in the cache and create a new connection, that's easiest
        FtpConnection slaveConnection = connectionCache.remove(currentLocation.getSlaveName());
        slaveConnection.disconnect(false); // this is just to keep things nice and clean
        slaveConnection = getSlaveConnection(currentLocation.getSlaveName()); // we don't return anything from here, but it gets put in the cache anyway
        boolean ok = slaveConnection.cwd(currentLocation.getSlavePath());
        if (!ok) {
            // this can happen if somebody removes the directory from the slave while you're still in it
            slaveConnection.disconnect(false);
            throw new IOException("No such directory");
        }
    }

    public boolean isRemote() {
        return currentLocation != null && currentLocation.isRemote();
    }

    @Override
    public void shutdown() {
        for (FtpConnection slave : connectionCache.values()) {
            slave.disconnect(false);
        }
        super.shutdown();
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public String setProperty(String property, String value) {
        return properties.put(property, value);
    }

    @Override
    public void rnfr(String source) throws PermissionDeniedException, AccessControlException, IOException {
        Location location = resolve(source, currentLocation);
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.RENAME, location.getPath(), user)) {
            throw new PermissionDeniedException(ActionPermission.RENAME, location.getPath(), user);
        }
        if (location.isRemote()) {
            try {
                FtpConnection slave = getSlaveConnection(location.getSlaveName());
                slave.send("RNFR " + location.getSlavePath());
                FtpResponse response = slave.getResponse();
                if (response.getCode() == 350) {
                    remoteRenameSource = location;
                } else {
                    throw new IOException(response.getFullMessage());
                }
            } catch (HostDisconnectedException e) {
	            // while you normally shouldn't concatenate the exception messages like this,
	            // we need to do it, because we're not dumping the stack trace, we're just
	            // printing the message
	            throw new IOException("Could not connect to slave: " + e.getMessage(), e);
            }
        } else {
            throw new IOException("Cannot rename non-slave entities");
        }
    }

    @Override
    public boolean rnto(String target) throws PermissionDeniedException, AccessControlException, IOException {
	    // todo: this isn't a very readable style. check for errors and throw close to the check first
        if (remoteRenameSource != null) {
            Location location = resolve(target, currentLocation);
            if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.RENAME, location.getPath(), user)) {
                throw new PermissionDeniedException(ActionPermission.RENAME, location.getPath(), user);
            }
            if (location.isRemote()) {
                if (location.getSlaveName().equals(remoteRenameSource.getSlaveName())) {
                    // source and destination are on the same slave, go ahead and rename
                    try {
                        FtpConnection slave = getSlaveConnection(location.getSlaveName());
                        slave.send("RNTO " + location.getSlavePath());
                        FtpResponse response = slave.getResponse();
                        if (response.getCode() == 250) {
                            location.getTree().setLastModified(System.currentTimeMillis()); // update the date so that the listings make sense. The Tree takes care of doing this recursively up to the root directory
                            return true;
                        } else {
                            throw new IOException(response.getFullMessage());
                        }
                    } catch (HostDisconnectedException e) {
                        throw new IOException(e.getMessage(), e);
                    }
                } else {
                    throw new IOException("Cannot rename between slaves, please specify a source and a target that resides on the same slave");
                }
            } else {
                throw new IOException("Cannot rename to a non-slave entity");
            }
        } else {
            throw new IOException("No rename source specified, please issue RNFR first");
        }
    }

    @Override
    public String mkd(String directory) throws PermissionDeniedException, AccessControlException, IOException {
        Location location = resolve(directory, currentLocation);
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.MKDIR, location.getPath(), user)) {
            throw new PermissionDeniedException(ActionPermission.MKDIR, location.getPath(), user);
        }
        if (location.isRemote()) {
            try {
                FtpConnection slave = getSlaveConnection(location.getSlaveName());
                slave.send("MKD " + location.getSlavePath());
                FtpResponse response = slave.getResponse();
                if (response.getCode() == 257) {
                    location.getTree().setLastModified(System.currentTimeMillis()); // update the date so that the listings make sense. The Tree takes care of doing this recursively up to the root directory
                    return location.getPath();
                } else {
                    throw new IOException(response.getFullMessage());
                }
            } catch (HostDisconnectedException e) {
	            // while you normally shouldn't concatenate the exception messages like this,
	            // we need to do it, because we're not dumping the stack trace, we're just
	            // printing the message
	            throw new IOException("Could not connect to slave: " + e.getMessage(), e);
            }
        } else {
            throw new IOException("Cannot create directories in non-slave entities");
        }
    }

    @Override
    public void rmd(String path) throws PermissionDeniedException, AccessControlException, IOException {
        Location location = resolve(path, currentLocation);
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.RMDIR, location.getPath(), user)) {
            throw new PermissionDeniedException(ActionPermission.RMDIR, location.getPath(), user);
        }
        if (location.isRemote()) {
            try {
                FtpConnection slave = getSlaveConnection(location.getSlaveName());
                slave.send("RMD " + location.getSlavePath());
                FtpResponse response = slave.getResponse();
                if (response.getCode() != 250) {
                    throw new IOException(response.getFullMessage());
                }
                location.getTree().setLastModified(System.currentTimeMillis()); // update the date so that the listings make sense. The Tree takes care of doing this recursively up to the root directory
            } catch (HostDisconnectedException e) {
	            // while you normally shouldn't concatenate the exception messages like this,
	            // we need to do it, because we're not dumping the stack trace, we're just
	            // printing the message
	            throw new IOException("Could not connect to slave: " + e.getMessage(), e);
            }
        } else {
            throw new IOException("Cannot create directories in non-slave entities");
        }
    }

    @Override
    public void delete(String path) throws PermissionDeniedException, AccessControlException, IOException {
        Location location = resolve(path, currentLocation);
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.DELETE, location.getPath(), user)) {
            throw new PermissionDeniedException(ActionPermission.DELETE, location.getPath(), user);
        }
        if (location.isRemote()) {
            try {
                FtpConnection slave = getSlaveConnection(location.getSlaveName());
                slave.send("DELE " + location.getSlavePath());
                FtpResponse response = slave.getResponse();
                if (response.getCode() != 250) {
                    throw new IOException(response.getFullMessage());
                }
                location.getTree().setLastModified(System.currentTimeMillis()); // update the date so that the listings make sense. The Tree takes care of doing this recursively up to the root directory
            } catch (HostDisconnectedException e) {
	            // while you normally shouldn't concatenate the exception messages like this,
	            // we need to do it, because we're not dumping the stack trace, we're just
	            // printing the message
                throw new IOException("Could not connect to slave: " + e.getMessage(), e);
            }
        } else {
            throw new IOException("Cannot create directories in non-slave entities");
        }        
    }

    @Override
    public long length(String file) throws FileNotFoundException {
        Location location = resolve(file, currentLocation);
        try {
            FtpConnection slaveConnection = getSlaveConnection(location.getSlaveName());
            slaveConnection.send("SIZE " + location.getSlavePath());
            FtpResponse response = slaveConnection.getResponse();
            if (response.getCode() == 213) {
                return Long.parseLong(response.getMessage().substring(4));
            } else {
                throw new FileNotFoundException(file);
            }
        } catch (IOException | HostDisconnectedException | NumberFormatException e) {
	        // NumberFormatException should never happen, but in case it does, we don't want cubnc to crash.
	        // it's better to be safe than sorry when dealing with use input.
            throw new FileNotFoundException(file);
        }
    }

    @Override
    public boolean isDirectory(String realPath) {
        try {
	        int colonIndex = realPath.indexOf(':');
	        // path starts with slave name. This means that the location MUST be on a slave
	        String slaveName = realPath.substring(0, colonIndex);
	        String slavePath = realPath.substring(colonIndex + 1);
	        FtpConnection slaveConnection = getSlaveConnection(slaveName);
            String previousDir = slaveConnection.getPwd();
            boolean isDirectory = slaveConnection.cwd(slavePath);
            slaveConnection.cwd(previousDir);
            return isDirectory;
        } catch (IOException | HostDisconnectedException e) {
	        // todo: this is actually VERY bad. it masks our failures...
	        // this is likely because the normal FileSystem doesn't throw from this method
            return false;
        } 
    }

    /**
     * Sends a site wipe to the slave. If the path contains a "-r", this is first removed before the slave
     * path is determined. If it contained a "-r", cubnc will send a "-r" to the slave.
     * @param path the cubnc path of the item to be wiped.
     * @return true if the wipe was successful, false otherwise.
     * @throws java.io.FileNotFoundException if some connection problem occurs.
     */
    public boolean wipe(String path) throws FileNotFoundException {
        path = path.trim();
        boolean recursiveFlag = false;
        if (path.startsWith("-r")) {
            path = path.substring(2).trim();
            recursiveFlag = true;
        }
        Location location = resolve(path, currentLocation);
        try {
            FtpConnection slaveConnection = getSlaveConnection(location.getSlaveName());
            slaveConnection.send("SITE WIPE " + (recursiveFlag ? "-r ": "") + location.getSlavePath());
            FtpResponse response = slaveConnection.getResponse();
            return response.getCode() == 200;
        } catch (IOException | HostDisconnectedException e) {
            throw (FileNotFoundException) new FileNotFoundException(path).initCause(e);
        }
    }

    /**
     * This takes a slave path and translates it to a cubnc path.
     * <p>path: /some/dir/Resulting.Dir<br>
     * slavePath: /some/<br>
     * realPath for that slave path: /dude/guy/<br>
     * return: /dude/guy/dir/Resulting.Dir<br>
     *
     * @param path the path to be translated.
     * @return a path that is usable in cubnc. null if no path could be found.
     */
    public String translateSlavePath(String path) {
        if (path == null) {
            return null;
        }

        for (Map.Entry<String, String> slavePathEntry : vtree.getSlavePathToRealPath().entrySet()) {
            if (path.startsWith(slavePathEntry.getKey())) {
                String partialPath = path.substring(slavePathEntry.getKey().length());
                String slavePath = slavePathEntry.getValue();
                if (slavePath.endsWith("/")) {
                    slavePath = slavePath.substring(0, slavePath.length() - 1);
                }
                if (partialPath.startsWith("/")) {
                    partialPath = partialPath.substring(1);
                }
                return slavePath + "/" + partialPath;
            }
        }
        return null;
    }
}
