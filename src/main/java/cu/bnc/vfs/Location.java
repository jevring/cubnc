/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.vfs;

import cu.ftp.Bnc;


/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-19 - 21:41:03
 */
public class Location {
    private final Tree tree;
    /**
     * This is the starting slave path, concatenated with the remainder of the specified path.
     * I.e. if the slave path is /x/, and it's aliased in /t/r/, and the path is /t/r/y
     * then the slavepath will be: /x/y
     */
    private final String slavePath;
    private final String path;
    private final String slaveName;
    private boolean exists;

    public Location(Tree tree, String slavePath, String path, String slaveName) {
        this.tree = tree;
        this.slavePath = slavePath;
        this.path = path;
        this.slaveName = slaveName;
    }

    public boolean isRemote() {
        return slaveName != null && !"".equals(slaveName);
    }

    public Tree getTree() {
        return tree;
    }

    public String getPath() {
        return path;
    }

    /**
     * This delivers the entire path on the slave for this location. It's a concatenation of the starting slave path
     * and the left-over path from the expression.
     * @return
     */
    public String getSlavePath() {
        return slavePath;
    }

    public String getSlaveName() {
        return slaveName;
    }

    public boolean exists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }

	@Override
	public String toString() {
		return "Location{" +
				"slavePath='" + slavePath + '\'' +
				", path='" + path + '\'' +
				", slaveName='" + slaveName + '\'' +
				", exists=" + exists +
				", remote=" + isRemote() +
				'}';
	}
}
