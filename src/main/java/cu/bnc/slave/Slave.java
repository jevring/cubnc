/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.slave;

import cu.ftp.Bnc;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-28 - 21:45:41
 */
public class Slave {
    protected final String name;
    protected final String host; // todo: take the multi-host concept from cubnc1's SlaveServer class
    protected final int port;
    protected final boolean acceptsIdnt;
    protected final boolean showWelcomeMessage;
    protected final int connectionTimeout;
    protected final int timeout;

    public Slave(String name, String host, int port, boolean acceptsIdnt, boolean showWelcomeMessage, int connectionTimeout, int timeout) {
        this.name = name;
        this.host = host;
        // optionals
        // NOTE: These are made optional "outside" of this class, in the VirtualFileSystem class.
        this.port = port;
        this.acceptsIdnt = acceptsIdnt;
        this.showWelcomeMessage = showWelcomeMessage;
        this.connectionTimeout = connectionTimeout;
        this.timeout = timeout;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public boolean acceptsIdnt() {
        return acceptsIdnt;
    }

    public boolean isShowWelcomeMessage() {
        return showWelcomeMessage;
    }

    @Override
    public String toString() {
        return "Slave{name=" + name + ";host=" + host + ";port=" + port + ";idnt=" + acceptsIdnt + ";welcome=" + showWelcomeMessage + ";connectionTimeout=" + connectionTimeout + "}";
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public int getTimeout() {
    return timeout;
}
}
