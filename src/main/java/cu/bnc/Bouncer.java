/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc;

import cu.ftpd.Server;
import cu.ftpd.Connection;
import cu.ftpd.ServiceManager;
import cu.ftpd.FtpdSettings;
import cu.ftpd.filesystem.permissions.PermissionConfigurationException;
import cu.ftpd.logging.Logging;
import cu.ftpd.logging.Formatter;
import cu.bnc.vfs.VirtualDirectoryTree;
import cu.bnc.vfs.VirtualFileSystem;
import cu.settings.ConfigurationException;
import cu.ssl.DefaultSSLSocketFactory;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.security.NoSuchAlgorithmException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.net.Socket;

// todo: modify the help-files to reflect what cubnc can do

// todo: make a feature list and a nice presentation of cubnc2
/*
todo: write a default command redirector, like in cubnc1

 */

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-18 - 17:41:17
 */
public class Bouncer extends Server {
    protected VirtualDirectoryTree vtree;

    protected Bouncer(String configurationFile) {
        super(configurationFile);
    }

    @Override
    protected void createConnection(Socket client) {
        Connection conn = new BouncerConnection(client, clientId.incrementAndGet(), vtree);
        // add it to the set of connections
        connections.add(conn); // thread safe via synchronized set above
        // start it
        conn.start();
    }

    @Override
    protected void configure() throws IOException, ConfigurationException, ClassNotFoundException, IllegalAccessException, InstantiationException, NotBoundException, NoSuchAlgorithmException, KeyManagementException, KeyStoreException, CertificateException, UnrecoverableKeyException, PermissionConfigurationException {
        FtpdSettings settings = new FtpdSettings("/bnc", configurationFile, "/bnc.xsd");
        vtree = new VirtualDirectoryTree(settings.getNode("/virtual_dirs"));

        System.setProperty("javax.net.ssl.trustStore", settings.get("/ssl/truststore/location"));
        System.setProperty("javax.net.ssl.trustStorePassword", settings.get("/ssl/truststore/password"));
        System.setProperty("javax.net.ssl.keyStore", settings.get("/ssl/keystore/location"));
        System.setProperty("javax.net.ssl.keyStorePassword", settings.get("/ssl/keystore/password"));

        // slaves need to be loaded before the userbase is loaded
        VirtualFileSystem.loadSlaves(settings);
        ServiceManager.setServices(new CubncServices(settings));

        Logging.initialize(settings);

        DefaultSSLSocketFactory.createFactory(settings.get("/ssl/keystore/location"), settings.get("/ssl/keystore/password"));
        // verify the truststore settings right away
        DefaultSSLSocketFactory.checkKeystorePassword(settings.get("/ssl/truststore/location"), settings.get("/ssl/truststore/password"));

        Formatter.initialize(settings);

        createServerSocket(settings.get("/main/bind_address"), settings.getInt("/main/port"), settings.getInt("/ssl/mode"));
    }

    @Override
    public String getVersion() {
        final Package pkg = Bouncer.class.getPackage();
        return pkg.getSpecificationTitle() + "-" + pkg.getSpecificationVersion() + " (" + pkg.getImplementationVersion() + " @ " + pkg.getImplementationTitle() + ") - [" + super.getVersion() + "]";
    }

    public static void main(String[] args) {
        String configurationFile = "data/bnc.xml";
        if (args.length == 1) {
            configurationFile = args[0];
            if ("-version".equalsIgnoreCase(args[0])) {
                Bouncer b = new Bouncer(configurationFile);
                System.out.println(b.getVersion());
                return;
            }
        }
        instance = new Bouncer(configurationFile);
        instance.init();
    }
}
