package cu.bnc;

import cu.ftpd.Services;
import cu.ftpd.FtpdSettings;
import cu.ftpd.filesystem.permissions.PermissionConfigurationException;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.settings.ConfigurationException;
import cu.bnc.user.userbases.glftpd.GlftpdUserbase;
import cu.bnc.user.userbases.host.RedundantHostUserbase;
import cu.bnc.commands.site.CubncSiteCommandHandler;

import java.io.IOException;
import java.io.File;
import java.rmi.NotBoundException;

/**
 * @author captain
 * @version $Id$
 * @since 2008-okt-27 - 23:01:45
 */
public class CubncServices extends Services {
    /*
    20=glftpd 2.01              (read-only, no groups or group permission available)
    21=glftpd 1.32              (read-only, no groups or group permission available)
    22=redundant host authenticator (read-only, only username permissions available, no ownership permissions available)
     */
    private static final int GLFTPD_201 = 20;
    private static final int GLFTPD_132 = 21;
    private static final int REDUNDANT_HOST_AUTHENTICATOR = 22;
    
    public CubncServices(FtpdSettings settings) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, NotBoundException, ConfigurationException, PermissionConfigurationException {
        this.settings = settings;
        // don't load the metadata handler, we don't need it
        configureCustomClassLoader();
        this.siteCommandHandler = new CubncSiteCommandHandler(settings);
        initializeCustomSiteCommands();
        configureEventHandler();
        configureUserbase();
        configureUserStatistics();
        configureModules();
        initializePermissions();
    }

    @Override
    protected void configureUserbase() throws ConfigurationException, NotBoundException, IOException {
        switch(settings.getInt("/user/authentication/type")) {
            case GLFTPD_201:
                System.out.println("Initializing GLFTPD 2.01 userbase...");
                if (settings.get("/user/authentication/glftpd/passwd_file_location") == null ||
                        settings.get("/user/authentication/glftpd/userfile_directory") == null) {
                    throw new ConfigurationException("must specify {passwd_file_location, userfile_directory} when using glftpd_201 userbase");
                }
                userbase = new GlftpdUserbase(GlftpdUserbase.GLFTPD_201, new File(settings.get("/user/authentication/glftpd/userfile_directory")), new File(settings.get("/user/authentication/glftpd/passwd_file_location")));
                break;
            case GLFTPD_132:
                System.out.println("Initializing GLFTPD 1.32 userbase...");
                if (settings.get("/user/authentication/glftpd/passwd_file_location") == null ||
                        settings.get("/user/authentication/glftpd/userfile_directory") == null) {
                    throw new ConfigurationException("must specify {passwd_file_location, userfile_directory} when using glftpd_132 userbase");
                }
                userbase = new GlftpdUserbase(GlftpdUserbase.GLFTPD_132, new File(settings.get("/user/authentication/glftpd/userfile_directory")), new File(settings.get("/user/authentication/glftpd/passwd_file_location")));
                break;
            case REDUNDANT_HOST_AUTHENTICATOR:
                System.out.println("Initializing REDUNDANT HOST userbase...");
                if (settings.get("/user/authentication/redundant_hosts/slaves") == null ||
                        settings.get("/user/authentication/redundant_hosts/host_check_interval") == null ||
                        settings.get("/user/authentication/redundant_hosts/ssl") == null) {
                    throw new ConfigurationException("must specify {slaves, host_check_interval, ssl} when using redundant hosts userbase");
                }
                userbase = new RedundantHostUserbase(settings.getInt("/user/authentication/redundant_hosts/host_check_interval"), settings.get("/user/authentication/redundant_hosts/slaves"), settings.getBoolean("/user/authentication/redundant_hosts/ssl"));
                break;
            default:
                super.configureUserbase();
                break;
        }
    }
}
