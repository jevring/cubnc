package cu.bnc.commands.site;

import cu.bnc.commands.site.actions.CubncXDupe;
import cu.bnc.commands.site.actions.GlobalRedirect;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.ftpd.commands.site.actions.*;
import cu.ftpd.FtpdSettings;
import cu.ftpd.Connection;
import cu.ftpd.user.userbases.actions.Who;
import cu.ftpd.user.userbases.actions.XWho;
import cu.bnc.BouncerConnection;
import cu.bnc.commands.site.actions.MultiSlaveExecute;
import cu.bnc.commands.site.actions.MultiSlaveSearch;

/**
 * @author captain
 * @version $Id$
 * @since 2008-nov-16 - 21:26:34
 */
public class CubncSiteCommandHandler extends SiteCommandHandler {
    public CubncSiteCommandHandler(FtpdSettings settings) {
        super(settings);
        this.registerAction("mexec", new MultiSlaveExecute());
        this.registerAction("msearch", new MultiSlaveSearch());
        this.registerAction("xdupe", new CubncXDupe());
    }

    protected void noSuchCommand(Connection connection, String parameters) {
        BouncerConnection bc = (BouncerConnection)connection;
        boolean relayed = bc.proxyCommand("SITE " + parameters);
        if (!relayed) {
            // we were local
            connection.respond("500 Unknown command: " + parameters);
        }
    }

	@Override
	public void registerCustomAction(String name, String path, String type, boolean addResponseCode) {
		if ("global".equals(type)) {
			registerAction(name, new GlobalRedirect(name, path, addResponseCode));
		} else {
			throw new IllegalArgumentException("Unknown command type: " + type);
		}
	}
}
