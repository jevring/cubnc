package cu.bnc.commands.site.actions;

import cu.bnc.BouncerConnection;
import cu.bnc.vfs.VirtualFileSystem;
import cu.ftpd.Connection;
import cu.ftpd.commands.site.actions.Xdupe;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;

/**
 * Performs xdupe handling while keeping track of connection state etc.
 *
 * @author markus@jevring.net
 */
public class CubncXDupe extends Xdupe {
	@Override
	public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
		if (connection instanceof BouncerConnection) {
			BouncerConnection bc = (BouncerConnection) connection;
			VirtualFileSystem vfs = bc.getVfs();
			if (vfs.getCurrentLocation().isRemote()) {
				if (parameterList.length > 1) {
					int txdupe = Integer.parseInt(parameterList[1]);
					if (txdupe < 5) {
						connection.setXdupe(txdupe);
						vfs.setProperty("XDUPE", parameterList[1]);
						bc.proxyCommand("SITE XDUPE " + parameterList[1]);
					} else {
						connection.respond("500 Unsupported xdupe mode.");
					}
				} else {
					// only get the status
					bc.proxyCommand("SITE XDUPE");
				}
			} else {
				super.execute(parameterList, connection, user, fs);
			}
		} else {
			super.execute(parameterList, connection, user, fs);
		}
	}
}
