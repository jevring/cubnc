package cu.bnc.commands.site.actions;

import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.Connection;
import cu.ftpd.logging.Formatter;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.bnc.vfs.VirtualFileSystem;
import cu.ftp.HostDisconnectedException;
import cu.ftp.FtpConnection;
import cu.ftp.FtpResponse;

import java.io.IOException;

/**
 * Sends a command to all the connected slaves (or a set of specified ones, in the future)<br>
 *
 *
 * In the future, we would have something like:<br>
 *
 * site spread {slave1, slave15, slave7:/usr/, slave34:/public_html} site search ponies!<br>
 * where "site search ponies!" is the command to send to all the slaves.
 *
 *
 * @author captain
 * @version $Id$
 * @since 2008-nov-16 - 22:22:22 (Hah! How cool is this! This is an automatically generated timestamp. How awesome!)
 */
public class MultiSlaveExecute extends Action {

    public MultiSlaveExecute() {
        super("mexec");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        VirtualFileSystem vfs = (VirtualFileSystem)fs;
        String command = Formatter.join(parameterList, 1, parameterList.length, " ");
        for (String slavename : VirtualFileSystem.getSlaves().keySet()) {
            try {
                FtpConnection slave = vfs.getSlaveConnection(slavename);
                slave.send(command);
                respond(slavename, connection, slave.getResponse());
                // send the command to the slave
                // respond to the user with a "200- XXX- result" where XXX is the reply code from the slave
            } catch (HostDisconnectedException e) {
                connection.respond("200 Slave unavailable: " + slavename);
            } catch (IOException e) {
                connection.respond("200 Slave unavailable: " + slavename);
            } finally {
                // disconenct the slaves?
                // if it is not necessary, people might not want this, if they want to prime the slaves by sending "site spread statline" or something to keep them all "warm".
            }
        }
        connection.respond("200 Command sent to slaves");
    }

    private void respond(String slavename, Connection connection, FtpResponse response) {
        //connection.respond("200- ::::::::::");
        //connection.respond("200- " + slavename);
        //connection.respond("200- ::::::::::");
        for (String line : response.getFullMessage().split("\\r\\n")) {
            //connection.respond("200- " + line);
            connection.respond("200- " + slavename + ": " + line);
        }
    }
}
