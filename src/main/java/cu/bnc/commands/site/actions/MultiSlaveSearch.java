package cu.bnc.commands.site.actions;

import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.Connection;
import cu.ftpd.logging.Formatter;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.bnc.vfs.VirtualFileSystem;
import cu.ftp.FtpConnection;
import cu.ftp.HostDisconnectedException;
import cu.ftp.FtpResponse;

import java.io.IOException;
import java.io.StringReader;
import java.io.BufferedReader;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * @author captain
 * @version $Id$
 * @since 2008-nov-16 - 23:11:48
 */
public class MultiSlaveSearch extends Action {

    public MultiSlaveSearch() {
        super("msearch");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        // NOTE: cubnc will take precedence in front of the sites, but it's probably rare to have cubnc->cubnc->site
        /*
        [L] site ioversion
        [L] 200-ioFTPD version: 6-9-0r
        [L] 200 'ioversion' Command successful.
        using nxtools as extended commands script and ioNINJA as sitebot
        ioFTPd:
        [L] site search DIR
        [L] 200- /path/to/DIRECTORY
        [L] 200 Command successful.
         */
        /*
        (20:30:10) [2] site vers
        (20:30:10) [2] 200 glFTPd 2.00 Linux+TLS
        (20:29:35) [2] site search dir
        (20:29:38) [2] 200- A total of YY hits were found! Found total of XXXXX of searchable entries.
        (20:29:38) [2] 200- /archive/dir
        (20:29:38) [2] 200- /archive/dir
        (20:29:38) [2] 200- /archive/dir
        (20:29:39) [2] 200- Max number of hits found (ZZZ), be more specific. Only displaying the first 200 hits
        (20:29:39) [2] 200 Command Successful.
        (21:20:31) [2] site search hej
        (21:20:34) [2] 200- A total of 3 hits were found! Found total of XXXXX of searchable entries.
        (21:20:34) [2] 200- /dir/names
        (21:20:34) [2] 200- /dir/names
        (21:20:34) [2] 200- /dir/names
        (21:20:34) [2] 200 Command Successful.
         */
        /*
        20:35 >>> SITE vers
        20:35 200 glFTPd 2.01 FreeBSD+TLS
         */
        /*
        (21:17:07) [2] site vers
        (21:17:07) [2] 200 glftpd 1.32_Linux+TLS
        (21:17:27) [2] site search dir
        (21:17:29) [2] 200- (Values displayed after dir names are Files/Megs/Age)
        (21:17:29) [2] 200- Doing case-insensitive search for 'dir':
        (21:17:29) [2] 200- /archive/somedir (97F/408.8M/739d 11h)
        (21:17:29) [2] 200- /archive/someotherdir (92F/202.9M/711d 20h)
        (21:17:30) [2] 200-  
        (21:17:30) [2] 200 99 directories found.

        glftpd 1.32 and cuftpd has size+files+age as well
         */

        long start = System.currentTimeMillis();
        VirtualFileSystem vfs = (VirtualFileSystem)fs;
        String command = Formatter.join(parameterList, 1, parameterList.length, " ");
        connection.respond("200- Searching all slaves for: " + command);
        int hits = 0;
        for (String slavename : VirtualFileSystem.getSlaves().keySet()) {
            try {
                FtpConnection slave = vfs.getSlaveConnection(slavename);
                // send "SITE VERSION" to check
                // if that didn't return anything useful, try "SITE VERS" (glftpd uses this)
                // OR we could try to find anything that looks like a path and translate it
                // This is probably simpler since we'll need to translate that anyway

                String version = determineVersion(slave);

                slave.send("SITE SEARCH " + command);
                FtpResponse response = slave.getResponse();
                if (response.getCode() == 200) {
                    hits += processResponse(response.getFullMessage(), connection, slavename, version, vfs);
                }
            } catch (HostDisconnectedException e) {
                connection.respond("200 Slave unavailable: " + slavename);
            } catch (IOException e) {
                connection.respond("200 Slave unavailable: " + slavename);
            }
        }

        connection.respond("200 Found " + hits + " hits in " + Formatter.duration((System.currentTimeMillis() - start) / 1000));
    }

    private String determineVersion(FtpConnection slave) throws HostDisconnectedException {
        String version = "generic";
        slave.send("SITE VERSION");
        FtpResponse response = slave.getResponse();
        if (response.getCode() == 200) {
            // check the result
            if (response.getMessage().indexOf("cuftpd") > -1) {
                version = "cuftpd";
            }
        } else {
            slave.send("SITE VERS");
            response = slave.getResponse();
            if (response.getMessage().indexOf("glftpd 1.32") > -1) {
                version = "glftpd-1.32";
            } /*else if (response.getMessage().indexOf("glftpd 2") > -1) {
                // 2.00 and 2.01 are the same when it comes to the output of this command
                version = "glftpd-2";
            }
            // glftpd 2 and ioftpd are the same as generic, since they just list the paths on a line
            */
        }
        return version;
    }

    /**
     * Processes the search results for each slave and interprets the slaves paths into cubnc paths.
     * Will currently match on the first match, as it is common to only have one path pointing into a slave dir.
     *
     * @param searchResult A string containing the lines of result.
     * @param connection the CLIENT connection, i.e. the connection to respond to.
     * @param slavename the name of the slave on which we send the command.
     * @param version which ftpd is the slave running.
     * @param vfs used to resolve the paths.
     * @throws java.io.IOException never, since it is just declared in a BufferedReader which wraps a StringReader.
     * @return the number of hits this search generated on this particular slave.
     */
    private int processResponse(String searchResult, Connection connection, String slavename, String version, VirtualFileSystem vfs) throws IOException {
        int hits = 0;
        Pattern path;
        if ("cuftpd".equals(version)) {
            path = Pattern.compile("(\\d{3}.*)\\s+(\\/.+?)(\\s+.*)");
        } else if ("glftpd-1.32".equals(version)) {
            path = Pattern.compile("(\\d{3}.*)\\s+(\\/.+?)(\\s*\\(.*\\))*+");
        } else {
            // generic
            path = Pattern.compile("(\\d{3}.*)\\s+(\\/.+?)");
        }
        BufferedReader lines = new BufferedReader(new StringReader(searchResult));
        String line;
        Matcher m;
        // note: this readLine() declares that it throws IOException, it will NEVER actually throw it.
        while ((line = lines.readLine()) != null) {
            m = path.matcher(line);
            if (m.matches()) {
                connection.respond("200- " + vfs.translateSlavePath(m.group(2)));
                hits++;
            }
        }
        return hits;
    }
}
