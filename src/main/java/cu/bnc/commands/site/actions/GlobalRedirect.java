package cu.bnc.commands.site.actions;

import cu.bnc.vfs.VirtualFileSystem;
import cu.ftp.FtpConnection;
import cu.ftp.FtpResponse;
import cu.ftp.HostDisconnectedException;
import cu.ftpd.Connection;
import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class GlobalRedirect extends Action {
	private final String slave;
	private final boolean addResponseCode;

	public GlobalRedirect(String name, String slave, boolean addResponseCode) {
		super(name);
		this.slave = slave;
		this.addResponseCode = addResponseCode;
	}

	@Override
	public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
		//System.out.println("Redirecting '" + name + "' to slave '" + slave + "'");
		if (fs instanceof VirtualFileSystem) {
			try {
				final FtpConnection slaveConnection = ((VirtualFileSystem) fs).getSlaveConnection(slave);
				slaveConnection.send("SITE " + name + " " + Formatter.join(parameterList, 1, parameterList.length, " "));
				final FtpResponse r = slaveConnection.getResponse();
				for (String line : r.getFullMessage().split("\\r\\n")) {
					if (addResponseCode) {
						connection.respond("200- " + line);
					} else {
						connection.respond(line);
					}
				}
				if (addResponseCode) {
					connection.respond("200 Complete");
				}

			} catch (IOException | HostDisconnectedException e) {
				// todo: do better than 500 here
				connection.respond("500 Could not redirect command to slave: " + e.getMessage());
			}
		} else {
			throw new IllegalAccessError("file system was not a virtual file system");
		}
	}
}
