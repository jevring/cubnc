/**
 * Copyright (c) 2005, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.user.userbases.host;

import cu.bnc.slave.Slave;

/**
 * A list entry for redundant hosts. Essentially keeps track of peoerties for the host.
 *
 * @author Markus Jevring <jevring@gmail.com>
 * @version 2005-jan-26
 */
public class RedundantHostEntry {
    private Slave slave;
    private String name;
    private boolean up;

    /**
     * Constructor, sets a couple of properties for the host.
     *
     * @param slave the <code>Slave</code> this host refers to
     * @param name the name of the host
     * @param up whether or not it is up at creation or now. very rarely supplied with false here.
     */
    public RedundantHostEntry(Slave slave, String name, boolean up) {
        this.slave = slave;
        this.name = name;
        this.up = up;
    }

    /* Getters and setters */
    public Slave getSlave() {
        return slave;
    }

    public String getName() {
        return name;
    }

    public boolean isUp() {
        return up;
    }

    /**
     * Flags this host as being down.
     */
    public void disable() {
        this.up = false;
    }

    /**
     * Flags this host as being back up.
     */
    public void enable() {
        this.up = true;
    }
}
