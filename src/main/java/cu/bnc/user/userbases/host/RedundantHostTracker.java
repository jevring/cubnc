/**
 * Copyright (c) 2005, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.user.userbases.host;

import java.util.*;
import java.net.*;
import java.io.IOException;

import cu.bnc.slave.Slave;
import cu.bnc.vfs.VirtualFileSystem;
import cu.settings.ConfigurationException;
import cu.ftpd.logging.Logging;

/**
 * This class keeps track of all the authentication hosts. It can deliver hosts for authentication modules, but it also keeps track of what hosts are up and what hosts are down.
 * If one or more hosts are down, it will iteratte over tham at a set interval and check if they can be labeled as "up" again.
 *
 * @author Markus Jevring <jevring@gmail.com>
 * @version 2005-jan-26
 */
public class RedundantHostTracker extends TimerTask {
    private final Map<String, RedundantHostEntry> redundantHostEntries = new HashMap<String, RedundantHostEntry>();

    /**
     * Constructor, reads the slaves from bnc.xml, and creates the list of authentication host objects.
     */
    public RedundantHostTracker(String slaveNames) throws ConfigurationException {
        if (slaveNames.length() != 0) {
            String[] sa = slaveNames.split(" ");
            for (int i = 0; i < sa.length; i++) {
                Slave slave = VirtualFileSystem.getSlave(sa[i].trim());
                if (slave != null) {
                    redundantHostEntries.put(sa[i], new RedundantHostEntry(slave, sa[i], true));
                } else {
                    throw new ConfigurationException("No such slave name: " + sa[i]);
                }
            }
        }
    }

    /**
     * This runs every n seconds (minutes mostly), and checks every downed host for connectability. If a host can be reached, its status is upgraded to "up" again.
     */
    public void run() {
        // check here if we have a host down. if we do, test it to see if we can bring it back up again, do so for all hosts, starting with the primary and moving down the hierarchy
        for (RedundantHostEntry rhe : redundantHostEntries.values()) {
            if (!rhe.isUp()) {
                // if it is down, check to see if it might actually be up. if it is, then mark it as up again, keep on going.
                try {
                    Socket sock = new Socket();
                    sock.connect(new InetSocketAddress(rhe.getSlave().getHost(), rhe.getSlave().getPort()), 10000);
                    // we can connect, that's all we want, right?
                    // if it doesn't throw one of the exceptions, it's qualified as being up again.
                    sock.close();
                    rhe.enable();
                } catch (IOException e) {
                    // never mind this, this means it just stays disabled.
                }
            }
        }
    }

    /**
     * Enables authentication moduels to report that a certain host they've just used is not reachable.
     * @param name the name of the downed host.
     */
    public void reportDownedHost(String name) {
        RedundantHostEntry rhe = redundantHostEntries.get(name);
        if (rhe != null) {
            rhe.disable();
        } else {
            Logging.getErrorLog().reportError("Trying to set a slave that doesn't exist (" + name + ") to down");

        }
    }

    /**
     * Gets the highest ranking authentication host from the list of hosts that are up.
     * @return the highest ranking online authentication host.
     */
    public Slave getHost() throws NoHostsAvailableException {
        for (RedundantHostEntry rhe : redundantHostEntries.values()) {
            if (rhe.isUp()) {
                return rhe.getSlave();
            }
        }
        throw new NoHostsAvailableException();
    }
}
