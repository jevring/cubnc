/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.user.userbases.host;

import cu.ftpd.user.userbases.*;
import cu.ftpd.user.userbases.anonymous.AnonymousUserbase;
import cu.ftpd.user.User;
import cu.authentication.AuthenticationRequest;
import cu.settings.ConfigurationException;
import cu.bnc.slave.Slave;
import cu.ftp.HostDisconnectedException;
import cu.ftp.FtpConnection;
import cu.ftp.FtpResponse;

import java.util.Timer;
import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-sep-14 - 15:23:00
 */
public class RedundantHostUserbase extends AnonymousUserbase {
    private final Timer timer = new Timer();
    private final RedundantHostTracker rht;
    private final boolean ssl;

    /**
     * Creates a userbase whose authentication is based on connecting to a host using the supplied credentials.
     * If those credentials are accepted, the user is authenticated and is provided with a dummy user object.
     *
     * @param interval how often (in seconds) that the hosts should be checked to see if they are up or not.
     * @param slaveNames the slave names in a string with spaces between them
     * @param ssl true if we should issue AUTH TLS before authentication.
     * @throws cu.settings.ConfigurationException when there is something wrong with the slave names, such as one of them not existing.
     */
    public RedundantHostUserbase(int interval, String slaveNames, boolean ssl) throws ConfigurationException {
        this.ssl = ssl;
        rht = new RedundantHostTracker(slaveNames);
        timer.schedule(rht, interval*1000, interval*1000);
    }

    @Override
    public User getUser(String username) throws NoSuchUserException {
        // _todo: implement me. (do we actually need to? can't we just use the AnonymousUser?)
        // yes, we need to do this, because otherwise the IP ranges will be wrong.
        // actually, no, with this we can't check ip-ranges, that can only be done by something that actually has the userbase!
        return super.getUser(username);
    }

    public int authenticate(AuthenticationRequest auth) {
        int result;
        try {
            boolean ok;
            while (true) {
                Slave slave = rht.getHost(); // this can get us out of the loop if we run out of hosts
                try {
                    FtpConnection client = new FtpConnection(slave.getHost(), slave.getPort(), auth.getUsername(), auth.getPassword(), ssl, 1, slave.getName());
                    ok = client.connect();
                    if (ok) {
	                    if (slave.acceptsIdnt()) {
		                    client.send("IDNT " + auth.getIdent() + "@" + auth.getHost().getHostAddress() + ":" + auth.getHost().getHostName());
		                    // pass false here, since send() above will already have invoked parseResponse()
		                    ok = (client.getResponse().getCode() == 220);
		                    if (ok) {
		                        ok = client.logon(false);
		                    } else {
			                    result = AuthenticationResponses.BAD_IDENT_OR_HOST;
			                    break;
		                    }
	                    } else {
		                    ok = client.logon(true);
	                    }
	                    if (ok) {
		                    result = AuthenticationResponses.OK;
		                    break;
	                    } else {
		                    result = AuthenticationResponses.BAD_PASSWORD;
		                    break;
	                    }
                    }
                    client.disconnect(false);
                } catch (HostDisconnectedException | IOException e) {
                    // just in case this was the last one, this is what we'll get
                    rht.reportDownedHost(slave.getName());
                    result = AuthenticationResponses.USERBASE_NOT_AVAILABLE;
                }
            }
        } catch (NoHostsAvailableException e) {
            result = AuthenticationResponses.USERBASE_NOT_AVAILABLE;
        }
        return result;
    }
}
