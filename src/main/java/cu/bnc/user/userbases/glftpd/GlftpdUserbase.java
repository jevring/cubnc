/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc.user.userbases.glftpd;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.AuthenticationResponses;
import cu.ftpd.user.userbases.Hex;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.PasswordHasher;
import cu.ftpd.user.userbases.anonymous.AnonymousUserbase;
import cu.settings.ConfigurationException;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-sep-16 - 11:55:27
 */
public class GlftpdUserbase extends AnonymousUserbase {
    public static final int GLFTPD_201 = 20;
    public static final int GLFTPD_132 = 21;
    private final int glftpdVersion;
    private final File userfileDirectory;
    private final File passwdFile;

    public GlftpdUserbase(int glftpdVersion, File userfileDirectory, File passwdFile) throws ConfigurationException {
        if (glftpdVersion != GLFTPD_201 && glftpdVersion != GLFTPD_132) {
            throw new ConfigurationException("glftpd version can only be '20' or '21'");
        }
        this.glftpdVersion = glftpdVersion;
        this.userfileDirectory = userfileDirectory;
        this.passwdFile = passwdFile;
    }

    public int authenticate(AuthenticationRequest auth) {
        //
        // READ USERFILE
        //

        UserInfo userInfo;
        try {
            userInfo = readUserFile(auth.getUsername());
        } catch (AuthenticationFailedException e) {
            return AuthenticationResponses.NO_SUCH_USER;
        }

        //
        // CHECK FLAGS
        //

        if (userInfo.getFlags().contains("6")) {
            return AuthenticationResponses.USER_SUSPENDED;
        }

        //
        // CHECK IDENT AND HOST
        //

        boolean identAndHostAllowed = false;
        String identAtHost = auth.getIdent() + "@" + auth.getHost().getHostName();
        String identAtIp = auth.getIdent() + "@" + auth.getHost().getHostAddress();

        boolean banned = false;
        for (String ip : userInfo.getIps()) {
            if (ip.startsWith("!")) {
                banned = true;
                ip = ip.substring(1);
            }
            ip = ip.replaceAll("\\.", "\\\\.").replaceAll("\\*", ".*").replaceAll("\\?", ".?");
            if (identAtIp.matches(ip) || identAtHost.matches(ip)) {
                if (banned) {
                    return AuthenticationResponses.BAD_IDENT_OR_HOST;
                } else {
                    identAndHostAllowed = true;
                    break;
                }
            }
        }

	    //
	    // CHECK EXPIRATION
	    //
	    if (glftpdVersion == GLFTPD_201 && userInfo.getExpires().before(new Date())) {
		    // we're piggybacking on the NO_SUCH_USER error here, as cu* doesn't have 
		    // expiration dates. This is kind-of correct, as there is *currently* no
		    // user with that name
		    return AuthenticationResponses.NO_SUCH_USER;
	    }

        if (!identAndHostAllowed) {
            return AuthenticationResponses.BAD_IDENT_OR_HOST;
        }

        //
        // CHECK PASSWORD
        //

        String hashedPassword = null;
        BufferedReader passwd = null;
        try {
            passwd = new BufferedReader(new InputStreamReader(new FileInputStream(passwdFile)));
            String line;
            while ((line = passwd.readLine()) != null) {
                if (line.startsWith("#")) continue;
                String[] parts = line.split(":");
                if (parts[0].equals(auth.getUsername())) {
                    hashedPassword = parts[1];
                    break;
                }
            }
        } catch (IOException e) {
            return AuthenticationResponses.USERBASE_NOT_AVAILABLE;
        } finally {
            if (passwd != null) {
                try {
                    passwd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (hashedPassword == null) {
            return AuthenticationResponses.NO_SUCH_USER;
        }

        //
        // Verify password
        //

        String hashedSuppliedPassword;
        // NOTE: there is a difference between 'hashedPassword' and 'hashedPasswordFromFile' in GLFTPD 2.01 but not in GLFTPD 1.32
        String hashedPasswordFromFile;
        if (glftpdVersion == GLFTPD_132) {
            hashedSuppliedPassword = Crypt.crypt(hashedPassword.substring(0, 1), auth.getPassword());
            hashedPasswordFromFile = hashedPassword;
            // the password contains the hash "built in", so to speak, whereas
            // the hash is split in the glftpd 2.01 case (like in cuftpd)
        } else { // if (glftpdVersion == GLFTPD_201) {
            String[] data = hashedPassword.split("\\$",3);
            if (data.length == 3) {
                String salt = data[1];
                hashedPasswordFromFile = data[2];
                // Note: we can't hash the password before we get in to here, which is cool when we're in system memory, but is more critical when we are doing RMI.
                // but since the RMI connection is protected by SSL, we get the same security as over SSH
                hashedSuppliedPassword = PasswordHasher.cuftpdHash(auth.getPassword().toCharArray(), Hex.hexToBytes(salt));
            } else {
                return AuthenticationResponses.BAD_PASSWORD;
            }
        }
        // compare just the password part, do not include 
        if (hashedPasswordFromFile.equals(hashedSuppliedPassword)) {
            return AuthenticationResponses.OK;
        } else {
            return AuthenticationResponses.BAD_PASSWORD;
        }
    }

    private UserInfo readUserFile(String username) throws AuthenticationFailedException {
        try (FileInputStream fis = new FileInputStream(new File(userfileDirectory, username))) {
            return readUserFile(fis);
        } catch (FileNotFoundException e) {
            throw new AuthenticationFailedException("Userfile not found", username, e, AuthenticationResponses.NO_SUCH_USER);
        } catch (IOException e) {
            throw new AuthenticationFailedException("Could not read userfile", username, e, AuthenticationResponses.NO_SUCH_USER);
        }
    }
	
	UserInfo readUserFile(InputStream stream) throws IOException {
		// package local to facilitate testing
		UserInfo userInfo = new UserInfo();
		BufferedReader in = new BufferedReader(new InputStreamReader(stream));
		String line;
		boolean groupSet = false;
		while((line = in.readLine()) != null) {
			String[] parts = line.split("\\s+");
			if ("FLAGS".equalsIgnoreCase(parts[0])) {
				userInfo.setFlags(parts[1]);
			} else if ("IP".equalsIgnoreCase(parts[0]) || "DNS".equalsIgnoreCase(parts[0])) {
				userInfo.addIp(parts[1]);
				// todo: we must be able to add the ips and dnss from linked files, as well as handle banned ips and dnss
				// we can do this when we check by examining "!" in the beginning of the IP or DNS
				// what we do need to do, howerver, is make sure that the most precise match wins, which can be tricky
				// maybe we shouldn't use regular expressions here, but rather parse and check for ourselves
				// ip has closest match to the right, dns has closest match to the left
				// or maybe we just say that matching one bad is stronger than matching any allow, regardless of how precise they are
			} else if ("TAGLINE".equalsIgnoreCase(parts[0])) {
				userInfo.setTagLine(Formatter.join(parts, 1, parts.length, " "));
			} else if ("GROUP".equals(parts[0])) {
				if (!groupSet) {
					userInfo.setGroup(parts[1]);
					groupSet = true;
				}
			} else if ("EXPIRES".equals(parts[0])) {
				Long time = Long.parseLong(parts[1]);
				if (time > 0) {
					Date expirationDate = new Date(time * 1000);
					userInfo.setExpires(expirationDate);
				}
			}
		}
		return userInfo;
	}

    @Override
    public User getUser(String username) throws NoSuchUserException {
	    
	    try {
		    UserInfo userInfo = readUserFile(username);
		    return new GlftpdUser(username, userInfo.getGroup(), userInfo.getTagLine());
	    } catch (AuthenticationFailedException e) {
		    // this shouldn't happen, as we should call authenticate() first.
		    // it can, however, if the userfile is deleted between authenticate()
		    // and getUser() is called
		    throw new NoSuchUserException(username);
	    }
    }

    class UserInfo {
	    // package local to facilitate testing
	    private final List<String> ips = new ArrayList<>();
	    private String flags;
	    private String tagLine;
	    private String group;
	    private Date expires = new Date(Long.MAX_VALUE);

        public void setFlags(String flags) {
            this.flags = flags;
        }

        public void addIp(String ip) {
            ips.add(ip);
        }

        public String getFlags() {
            return flags;
        }

        public List<String> getIps() {
            return ips;
        }

	    public String getTagLine() {
		    return tagLine;
	    }

	    public void setTagLine(String tagLine) {
		    this.tagLine = tagLine;
	    }

	    public String getGroup() {
		    return group;
	    }

	    public void setGroup(String group) {
		    this.group = group;
	    }

	    public Date getExpires() {
		    return expires;
	    }

	    public void setExpires(Date expires) {
		    this.expires = expires;
	    }
    }
}
