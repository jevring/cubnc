package cu.bnc.user.userbases.glftpd;

import cu.ftpd.user.userbases.anonymous.AnonymousUser;

/**
 * A user represented by the glftpd userfile. This is a read-only user, and is only used to extract 
 * information like group and tagline for use in events.
 *
 * @author markus@jevring.net
 */
public class GlftpdUser extends AnonymousUser {
	private final String group;
	private final String tagLine;
	
	public GlftpdUser(String username, String group, String tagLine) {
		super(username);
		this.group = group;
		this.tagLine = tagLine;
	}

	@Override
	public String getPrimaryGroup() {
		return group;
	}

	@Override
	public String getTagline() {
		return tagLine;
	}
}
