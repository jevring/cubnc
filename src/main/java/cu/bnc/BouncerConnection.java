/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.bnc;

import cu.bnc.slave.Slave;
import cu.ftpd.Connection;
import cu.ftpd.ServiceManager;
import cu.ftpd.commands.transfer.DataConnectionListing;
import cu.ftpd.filesystem.permissions.ActionPermission;
import cu.bnc.vfs.VirtualDirectoryTree;
import cu.bnc.vfs.VirtualFileSystem;
import cu.bnc.vfs.Location;
import cu.ftp.HostDisconnectedException;
import cu.ftp.FtpResponse;
import cu.ftp.FtpConnection;

import javax.net.ssl.SSLSocket;
import java.net.Socket;
import java.io.IOException;
import java.io.FileNotFoundException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2008-maj-19 - 22:41:59
 */
public class BouncerConnection extends Connection {
    protected VirtualDirectoryTree vtree;
    protected VirtualFileSystem vfs;
    protected String password;

    public BouncerConnection(Socket controlConnection, long connectionId, VirtualDirectoryTree vtree) {
        super(controlConnection, connectionId);
        this.vtree = vtree;
    }

    @Override
    protected void createAndSetFileSystem() {
        vfs = new VirtualFileSystem(user, vtree, password, controlConnection instanceof SSLSocket, auth); // checking instanceof instead of looking if a command has been issued or not lets use handle the ftps case as well
        fs = vfs;
        // NOTE: Since we don't allow any commands that might require interaction with the FileSystem before we have logged in (and the FileSystem has been created), we never need to check it for null when proxying
    }

    @Override
    protected void handlePrivilegedCommand(String command, String parameters) {
        // NOTE: we removed ABOR checking from here, since we check that as a special case in handleCommand(...)
        if ("RETR".equals(command) || "STOR".equals(command) || "APPE".equals(command)) {
            handleTransferCommand(command, parameters);
// above never works when we are local, not even if the file resolves properly. these MUST always fail unless we are remote and they actually resolve to remote files
        } else if ("XMD5".equals(command) || "XSHA1".equals(command) || "XCRC".equals(command) || "SIZE".equals(command) || "MLST".equals(command) || "MLSD".equals(command)) {
            try {
                proxyCommandWithFilename(command, parameters);
            } catch (FileNotFoundException e) {
                respond("550 File not found: " + parameters);
            }
// if resolved we should just connect to that slave and issue the command
        } else if ("DELE".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            dele(parameters);
        } else if ("RMD".equals(command) || "XRMD".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            rmd(parameters);
        } else if ("MKD".equals(command) || "XMKD".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            mkd(parameters);
        } else if ("RNFR".equals(command)) {
            rnfr(parameters);
        } else if ("RNTO".equals(command)) {
            rnto(parameters);
// above should all fail locally and pass on TRANSLATED remotely (if the translation is in the same slavedir or below)
        } else if ("QUIT".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            quit();
        } else if ("CWD".equals(command) || "XCWD".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            cwd(parameters);
        } else if ("PWD".equals(command) || "XPWD".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            pwd();
        } else if ("CDUP".equals(command) || "XCUP".equals(command)) {
            // let these be handled in the very normal way. the FS will take care of the necessary connection details
            cwd("..");
        } else if ("SYST".equals(command)) {
            // only ever ask about the SYST for cubnc
            syst();
        } else if ("SSCN".equals(command)) {
            vfs.setProperty("SSCN", parameters);
            sscn(parameters);
        } else if ("PBSZ".equals(command)) {
            vfs.setProperty("PBSZ", parameters);
            pbsz(parameters);
        } else if ("PROT".equals(command)) {
            vfs.setProperty("PROT", parameters);
            prot(parameters);
        } else if ("CLNT".equals(command)) {
            vfs.setProperty("CLNT", parameters);
            clnt(parameters);
        } else if ("NLST".equals(command)) {
            try {
                Location location = vfs.resolve(parameters, vfs.getCurrentLocation());
                if (location.isRemote()) {
                    proxyCommand(command + " " + location.getSlavePath(), location.getSlaveName());
                } else {
                    nlst(parameters);
                }
            } catch (FileNotFoundException e) {
                respond("500 No such directory: " + parameters);
            }
        } else if ("SITE".equals(command)) {
            site(parameters);
        } else {
            // these all have matched local and remote versions
            if (vfs.isRemote()) {
                proxyCommand(command + (parameters != null ? " " + parameters : ""), null);
            } else {
                // these are all commands that should be handled normally if we are in the bnc,
                // and should be proxied right away if we are connected to a slave
                if ("NOOP".equals(command)) {
                    noop();
                } else if ("MODE".equals(command)) {
                    // todo: solve this so we don't screw up. Maybe use this fs.setProperty for all fs things, even in cuftpd
                    // todo: only set the property if the command is successful
                    // actually, it might be better to remove it and make all the properties first-order members of the Connection and the FileSystem as applicable
                    // this makes much more sense from an OO point of view.
                    vfs.setProperty("MODE", parameters);
                    mode(parameters);

                    // todo: did we ever solve the type A not being able to transfer/fxp stuff? (no, it doesn't look like it, according to the changelog)

                    // what is the problem here? is it that we send a MODE command to a site, then connect to another site, and have the wrong mode? (or whatever other state we're looking at)?

                } else if ("REST".equals(command)) {
                    vfs.setProperty("REST", parameters);
                    rest(parameters);
                } else if ("TYPE".equals(command)) {
                    vfs.setProperty("TYPE", parameters);
                    type(parameters);
                } else if ("PORT".equals(command)) {
                    port(parameters);
                } else if ("EPRT".equals(command)) {
                    eprt(parameters);
                } else if ("PASV".equals(command)) {
                    pasv();
                } else if ("EPSV".equals(command)) {
                    epsv(parameters);
                } else if ("LIST".equals(command)) {
                    list(parameters, DataConnectionListing.LIST);
                } else if ("STAT".equals(command)) {
                    stat(parameters);
                } else if ("CPSV".equals(command)) {
                    cpsv();
                } else if ("FEAT".equals(command)) {
                    // we had to put this here, otherwise we're get a 500-reply too many, and everything would be skewed
                    feat();
                } else if ("HELP".equals(command)) {
                    help();
                } else if ("STOU".equals(command)) {
                    stou();
                } else {
                    respond("500 Unknown command: " + command);
                }
            }
        }
    }

	@Override
	protected void rnfr(String source) {
		try {
			Location location = vfs.resolve(source, vfs.getCurrentLocation());
			if (location.isRemote()) {
				super.rnfr(source);
			} else {
				respond("550 Cannot rename virtual directories");
			}
		} catch (FileNotFoundException e) {
			respond("550 File not found");
		}
	}

    @Override
    protected void prot(String mode) {
        // if we've already logged on to the slave, but haven't sent this command
        // yet, we have to send it now.
        // todo: it's likely that we have to do this for all the state changing commands.
        if (vfs.getCurrentLocation().isRemote()) {
            FtpResponse response = proxyCommand("PROT " + mode, vfs.getCurrentLocation().getSlaveName());
            if (response.getCode() >= 200 && response.getCode() < 300) {
                // this stuff is normally in super.prot(), but since that also
                // sends a response to the client, we can't simply execute that.
                if ("C".equalsIgnoreCase(mode)) {
                    encryptedDataConnection = false;
                } else if ("P".equalsIgnoreCase(mode)) {
                    encryptedDataConnection = true;
                }
                createControlStreams();
            } // else the client has gotten the error from the slave
        } else {
            super.prot(mode);
        }
    }

    @Override
    protected void pass(String password) {
        this.password = password;
        super.pass(password);
        // just save the password since it will be used whenever we connect to a slave
    }

    protected void handleTransferCommand(String command, String parameters) {
        // _todo: what about events for upload and download? (don't send any, see design_decisions.txt)
        // todo: only do Tree.setLastModified() if the transfer was actually successful (how do we know this?)
        try {
            Location location = vfs.resolve(parameters, vfs.getCurrentLocation());
            int permission;
            if ("RETR".equalsIgnoreCase(command)) {
                permission = ActionPermission.DOWNLOAD;
            } else {
                permission = ActionPermission.UPLOAD;
            }
            if (ServiceManager.getServices().getPermissions().hasPermission(permission, location.getPath(), user)) {
                if (location.isRemote()) {
                    // todo: will we run in to problems here with a full path name sent in the root?
                    // for instance, doing STOR /kek/lol.zip, after having gotten a PASV response
                    // indicating a port on the bnc, will obviously not work
                    proxyCommand(command + " " + location.getSlavePath(), location.getSlaveName());
                } else {
                    respond("500 You must enter the directory in question before issuing this command");
                    // we need to stop listenign and close connections here, because if PASV or PORT was issued when we were in a BNC dir, then we created those ports with the intent of transferring listings on them, which will now not happen.
                    stopListening();
                    try {
                        getDataConnection().close();
                    } catch (IOException e) {
                        // ignore
                        e.printStackTrace();
                    }
                }
            } else {
                respond("531 Permission denied.");
            }
        } catch (FileNotFoundException e) {
            respond("550 File not found: " + parameters);
        }
    }

    protected void proxyCommandWithFilename(String command, String filename) throws FileNotFoundException {
        Location location = vfs.resolve(filename, vfs.getCurrentLocation());
        if (location.isRemote()) {
            // this is remote
            proxyCommand(command + " " + location.getSlavePath(), location.getSlaveName());
        } else {
            // this is local
            respond("500 This command can only be applied to files or directories on slaves.");
        }
    }

    /**
     * Sends the command to the currently connected slave. Simplified usage for classes outside the BouncerConnection,
     * like the CubncSiteCommandHandler.
     *
     * @param command the command to be proxied
     * @return true if the command was sent, false if we are not currently connected to a slave
     */
    public boolean proxyCommand(String command) {
        if (vfs.getCurrentLocation().isRemote()) {
            // this is remote
            proxyCommand(command, vfs.getCurrentLocation().getSlaveName()); // assume that this will be executed in whatever slave dir we are already in
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sends a command to the specified slave. One failure triggers a reconnect and retry. If the retry fails, the command fails.
     *
     * @param command the command to be sent.
     * @param slaveName the slave to which to send the command. <code>null</code> indicates the current slave.
     */
    protected FtpResponse proxyCommand(String command, String slaveName) {
        boolean retry = false;
        try {
            return tryProxyCommand(command, slaveName);
        } catch (HostDisconnectedException | IOException e) {
            retry = true;
        }
        if (retry) {
            // retry will always be true here, because I changed this to return something.
            // keep it this way for the time being, though, for semantic clarity.
            // fix it up later, though.
            try {
                vfs.reconnect();
                return tryProxyCommand(command, slaveName);
            } catch (HostDisconnectedException | IOException e) {
                // still didn't work, send the error
                respond("500 Remote failure: " + e.getMessage());
            }
        }
        return new FtpResponse("Remote failure", 500, false, "Remove failure");
    }

    /**
     * Tries to send a command to the specified slave.
     *
     *
     * @param command the command to be sent.
     * @param slaveName the slave to which to send the command. <code>null</code> indicates the current slave.
     * @throws cu.ftp.HostDisconnectedException If the underlying slave has disconnected.
     * @throws java.io.IOException if an IO problem occurs.
     */
    protected FtpResponse tryProxyCommand(String command, String slaveName) throws IOException, HostDisconnectedException {
        FtpConnection slave;
        if (slaveName == null) {
            slave = vfs.currentSlave();
        } else {
            // this is used when we issue transfer commands
            slave = vfs.getSlaveConnection(slaveName);
        }
        slave.send(command);
        FtpResponse response = slave.getResponse();
        if (response.getFullMessage().endsWith("\r\n")) {
            // since respond() adds "\r\n", we need to remove it
            respond(response.getFullMessage().substring(0, response.getFullMessage().length() - 2));
        } else {
            respond(response.getFullMessage());
        }
        if (response.getCode() > 99 && response.getCode() < 200) {
            // messages in this class indicate that another response will be following it when this is over, so we need to wait for output again
            // use parseResponse() this time, otherwise we just get the previous response
            response = slave.parseResponse();
            if (response.getFullMessage().endsWith("\r\n")) {
                // since respond() adds "\r\n", we need to remove it
                respond(response.getFullMessage().substring(0, response.getFullMessage().length() - 2));
            } else {
                respond(response.getFullMessage());
            }
        }
        return response;
    }

    @Override
    protected void feat() {
        respond("211- Extensions supported");
        respond(" AUTH");
        respond(" SSCN");
        respond(" CPSV");
        respond(" PROT");
        respond(" PBSZ");
        respond(" CLNT");
        respond(" REST");
        respond(" REST STREAM");
        respond(" SIZE");
        respond(" EPRT");
        respond(" EPSV");
        respond(" NLST");
        respond(" SITE XDUPE {0,1,2,3,4}");
        respond("211 End.");
    }

    @Override
    public void statline(long speed) {
        // do nothing
        // this already comes from the slave when we are connected to a slave
        respond("226 cubnc ok.");
    }

    public VirtualFileSystem getVfs() {
        return vfs;
    }
}
