Syntax: site wkup [-number=n] [-section=name] [-raw=b]

Displays the upload statistics for the past week
Requires: UserPermission.STATISTICS
See also: alldn, allup, mndn, mnup, wkdn, wkup, daydn, dayup
Options:
-number=n       where 'n' is the number of entries to see

-section=name   where 'name' is the name of the section
                for which we want to display information
                
-raw=b          where 'b' is 'true' or 'false'. If set to 'true',
                this option presents the data in a format
                that is easier for machines to parse
