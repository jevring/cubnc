Syntax: site rufg username group

Removes user 'username' from group 'group'

Requires: UserPermission.GROUPS