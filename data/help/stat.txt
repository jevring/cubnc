Syntax: site stat

Displays the statline for the user, containing 
information about section and credits etc.