Syntax: site permissions

Lists the available numerical permissions on
the system. Useful for translating the user
permissions needed for a certain command.