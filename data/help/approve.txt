Syntax: site approve number

Approved the request indicated by the number 'number'.
A list of requests can be aquired by executing 'site requests'
See also: request, requests, reqfilled