#######################################################################################################################
# Any line that starts with a # is a comment.
#
# There is an implicit 'allow all in "/"' rule in the beginning, meaning that if no rule is matched,
# the action will be allowed (this is to allow the ftpd to run without setting up any permissions)
# It is good practise to specify a 'deny all in "/"' rule explicitly in the beginning of the ruleset,
# and then explicitly allow the things that are allowed.
# As a rule, give permissions only to groups, and in special cases, give them to users
#
# ALIASES:
# Aliases can be defined by connecting the alias to the value by an '=' sign.
# Aliases can be used in the rules below where it says $ACTION, $ENTITIES, $SUBJECTS and $OBSERVERS
# To use an alias in a rule, just prefix the alias name with a '$' sign.
# Examples:
# sections = {"/tv", "/mp3"}
# admins = {user:captain, group:admins}
# directory_actions = {mkd, cwd,rmdir, list}
# allow $directory_actions in $sections by $admins
#
# QUICK:
# http://www.openbsd.org/faq/pf/filter.html
# Have the notion of "quick" from pf
# If 'quick' is found, stop parsing and use that rule, otherwise the last rule to match is the winner
# The optional 'quick' in the rules below tells the engine to stop parsing rules if a rule with quick
# is matched, and use that rule.
#
# PATH:
# In the rules below, $PATH can be a path in quotes relative to the ftpd root, an alias,
# or a set of paths in the form {"/pub", "/hidden" }
#
# ENTITIES:
# $ENTITIES are either "group:groupname", "user:username" to define a single user or a single group
# or "{group:groupA, user:someuser, group:groupB}" to specify several users and/or groups
# The operator '*' can be used as "group:*" or "user:*" to indicate ALL or ANY group or user, respectively
# If the $ENTITIES are omitted from some rule, it means that that rule applies to ALL USERS & ALL GROUPS
#
# ALLOW/DENY:
# Actions: delete deleteown mkdir rmdir wipe cwd list upload fxpupload download fxpdownload
#          rename renameown resume resumeown nuke dupecheck settime all (meaning all the permissions)
# NOTE: fxpupload and fxpdownload check the intended data connection host, and allows or disallows based
#       on if this is the same as the control connection or not.
# Logging: dirlog dupelog xferlog
#
# Format: Allow: allow [quick] $ACTION [in $PATH] [by $ENTITIES]
#          Deny:  deny [quick] $ACTION [in $PATH] [by $ENTITIES]
# where $ACTION can be either Actions or Logging as specified above.
# NOTE: For logging permissions, the semantics for "allow" are that logging is required
#
# HIDE/SHOW:
# This hides or shows users and groups in listings.
#
# Format: Hide: hide [quick] $SUBJECTS [in $PATH] from $OBSERVERS
#         Show: show [quick} $SUBJECTS [in $PATH] to $OBSERVERS
# where $SUBJECTS are EITHER the people being watched (specified as $ENTITIES above) OR
#        directories and/or files that you want to hide in dir listings.
#       directories are indicated by "dir:dirname", and files with "file:filename".
#       They can still be aliased and put into lists {}, just like users and groups.
#       You can specify users, groups, dirs and files in the same rule, if you feel like it.
#   and $OBSERVERS are the people watching (specified as $ENTITIES above)
# NOTE: $SUBJECTS and $OBSERVERS cannot be omitted.
# As it is bad to hide things by default, here are some examples:
# hide dir:groups in "/" from user:*
# show dir:groups in "/" to group:affils
#
# INCLUDE:
# Other acl-files can be included as well. These files are included in the order they are found and can make use of
# aliases defined before them, just like other rules.
# Format: include filename.acl
#
# RESTRICT / UNRESTRICT
# This adds or removes transfer speed restrictions on users based on location, time-of-day, or just user/group name
# Format:   Restrict:   restrict [quick] [$ENTITIES] [in $PATH] [between $TIME and $TIME] to [$UPSPEED] and [$DOWNSPEED]
#         Unrestrict: unrestrict [quick] [$ENTITIES] [in $PATH] [between $TIME and $TIME]
# where $TIME is HH:MM where HH is hour and MM is minute
#   and $UPSPEED   is "$SPEED up"
#   and $DOWNSPEED is "$SPEED down"
# where $SPEED is a value representing the speed limit in bytes per second. The suffixes 'k' or 'm' can be used
# to denote "kilobytes per second" and "megabytes per second" respectively.
# It is not necessary to specify both upload speed and download speed, but at least one should be specified.
# If only one is specified, the "and" should be omited. If both are specified, then the upload speed must be specified
# before the "and", and the download speed after.
# "unrestrict" does not, obviously, require any speed to be specified, and in fact should not have one.
# As there are no "sane defaults" with regards to speed restrictions, the permissions will not contain any, however
# there will be some examples in commens below.
# Variables in brackets are optional. $ENTITIES and $PATHS work as for 'allow' and 'deny', and as such, they can
# also use aliases as previously described. Also as previously, the $ENTITIES can be removed for the rule to
# apply to everybody (making rules like "unrestrict quick" possible).
# Examples:
# restrict $admins between 12:00 and 18:00 to 256k down
# restrict user:captain in "/" between 12:00 and 24:00 to 1m down
# unrestrict {user:captain, group:admins} in "/admin"
#
#######################################################################################################################

allow quick all in "/" by user:*

sections = {"/mp3", "/tv", "/divx"}
admins = {user:captain, group:admins}
normal_operations = {cwd, mkdir, rmdir, upload, download}
logging = {dirlog, dupelog, xferlog}

# 'quick' here means that this is the only rule that applies to admins, which means that they can do anything anywhere.
allow quick all in "/" by $admins
allow $normal_operations in $sections

hide $admins in "/" from user:*
show $admins in "/admin" to $admins

deny all in "/groups"
deny $logging in {"/request", "/groups", "/admin" }
deny cwd in "/admin"

# use for special group dir settings and such, for example.
#include groups.acl

allow quick {mkdir, upload, download, cwd} in "/request"
allow quick nuke in "/" by {group:nukers}

allow $normal_operations in "/pub"
allow delete in "/pub"

deny dirlog in "/groups/admin"
deny xferlog in "/requests/" by group:admins



